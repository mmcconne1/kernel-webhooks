"""Tests for the Session and Handler objects."""
from copy import deepcopy
import json
from typing import NamedTuple
from unittest import TestCase
from unittest import mock

from cki_lib.misc import get_nested_key
from jira.exceptions import JIRAError

from tests import fake_payloads
from tests import fakes
from webhook import session
from webhook.common import get_arg_parser
from webhook.rh_metadata import Projects


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestSessionFunctions(TestCase):
    """Tests for the new_session and get_session functions."""

    def setUp(self):
        """Reset the SESSION global to None."""
        session.SESSION = None

    def tearDown(self):
        """Reset the SESSION global to None."""
        session.SESSION = None

    def test_new_base_session(self):
        """Returns a new BaseSession object and sets the SESSION global."""
        test_session = session.new_session('bughook')
        self.assertIsInstance(test_session, session.BaseSession)
        self.assertIs(test_session, session.SESSION)
        self.assertIs(test_session, session.get_session())

    def test_new_session_runner(self):
        """Returns a new SessionRunner object and sets the SESSION global."""
        handlers = {session.MessageType.AMQP: mock.Mock(name='amqp_handler_func')}
        test_session = session.new_session('buglinker', handlers=handlers)
        self.assertIsInstance(test_session, session.SessionRunner)
        self.assertIs(test_session, session.SESSION)
        self.assertIs(test_session, session.get_session())

    def test_get_session(self):
        """Raises a RuntimeError when the SESSION global is not set."""
        with self.assertRaises(RuntimeError):
            session.get_session()


class TestHelpers(TestCase):
    """Tests for the extra functions."""

    def test_make_logging_extras_amqp(self):
        """Returns a dict of some extra logging values."""
        headers = {}
        tests = [
            # (expected_result, body)
            # amqp-bridge event happy with nothing
            ({'bugzilla_id': None, 'bugzilla_user': None}, {}),
            # amqp-bridge event with something to say
            ({'bugzilla_id': 123456, 'bugzilla_user': 'user@example.com'},
             {'event': {'bug_id': 123456, 'user': {'login': 'user@example.com'}}}),
        ]

        for expected_result, body in tests:
            with self.subTest(expected_result=expected_result, body=body):
                self.assertEqual(
                    expected_result,
                    session.make_amqp_logging_extras(headers, body)
                )

    def test_make_logging_extras_umb(self):
        """Returns a dict of some extra logging values."""
        tests = [
            # (expected_result, headers, body)
            # umb-bridge event happy with nothing
            ({}, {}, {}),
            # umb-bridge event with something to say
            ({'mr_id': 3456, 'path_with_namespace': 'group/project', 'event_source': 'bugzilla'},
             {'source': 'bugzilla'},
             {'mrpath': 'group/project!3456'})
        ]

        for expected_result, headers, body in tests:
            with self.subTest(expected_result=expected_result, headers=headers, body=body):
                self.assertEqual(
                    expected_result,
                    session.make_umb_bridge_logging_extras(headers, body)
                )


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestBaseSessionInit(TestCase):
    """Tests for the initilization of the BaseSession class."""

    ENV_NAMES = ['development', 'staging', 'production']

    NATTRS = {'body': 'This should be unique per webhook\n\nEverything is fine, I swear...',
              'author': {'username': 'shadowman'},
              'id': '1234'}

    @mock.patch.object(Projects, 'do_load_policies')
    def test_init_webhook_exists(self, mock_do_load_policies):
        """Sets up a new BaseSession object and reports the correct environment."""
        # List of webhook names to create sessions for.
        defined_webhooks = list(Projects().webhooks.keys())
        self.assertGreater(len(defined_webhooks), 3)

        # Loop over the good webhook names and create/test a BaseSession
        for webhook_name in defined_webhooks:
            for mock_env in ('development', 'production', 'staging'):
                with self.subTest(mock_env=mock_env, webhook_name=webhook_name):
                    # Reset Projects.do_load_policies() mock.
                    mock_do_load_policies.reset_mock()
                    with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': mock_env}):
                        test_session = session.BaseSession(webhook_name, 'args',
                                                           load_policies=True)
                        test_session.rh_projects.do_load_policies.assert_called()
                        self.assertEqual(test_session.environment, mock_env)
                        match mock_env:
                            case 'development':
                                self.assertFalse(test_session.is_production)
                                self.assertFalse(test_session.is_staging)
                                self.assertFalse(test_session.is_production_or_staging)
                                self.assertIn('env: development', str(test_session))
                            case 'production':
                                self.assertTrue(test_session.is_production)
                                self.assertFalse(test_session.is_staging)
                                self.assertTrue(test_session.is_production_or_staging)
                                self.assertIn('env: PRODUCTION', str(test_session))
                            case 'staging':
                                self.assertFalse(test_session.is_production)
                                self.assertTrue(test_session.is_staging)
                                self.assertTrue(test_session.is_production_or_staging)
                                self.assertIn('env: staging', str(test_session))

                        gl_mergerequest = mock.Mock()
                        discussion = mock.Mock(attributes={'notes': [self.NATTRS]})
                        # discussion.notes.get.return_value = '1234'
                        gl_mergerequest.discussions.list.return_value = [discussion]
                        bot_name = "shadowman"
                        identifier = "This should be unique per webhook"
                        new_comment = "This is my new comment"
                        with self.assertLogs('cki.webhook.session', level='DEBUG') as logs:
                            test_session.update_webhook_comment(
                                gl_mergerequest, new_comment, bot_name=bot_name,
                                identifier=identifier)
                            if test_session.is_production_or_staging:
                                self.assertIn("Overwriting existing webhook comment",
                                              ' '.join(logs.output))
                            else:
                                self.assertIn("Would overwrite existing webhook comment",
                                              ' '.join(logs.output))
                            gl_mergerequest.notes.create.assert_not_called()
                        bot_name = "a_different_user"
                        with self.assertLogs('cki.webhook.session', level='DEBUG') as logs:
                            test_session.update_webhook_comment(
                                gl_mergerequest, new_comment, bot_name=bot_name,
                                identifier=identifier)
                            if test_session.is_production_or_staging:
                                self.assertIn("Creating new webhook comment", ' '.join(logs.output))
                            else:
                                self.assertIn("Would create new webhook comment",
                                              ' '.join(logs.output))
                            if test_session.is_production_or_staging:
                                gl_mergerequest.notes.create.assert_called()

    def test_init_webhook_unknown(self):
        """Raises a RuntimeError when the webhook name is not found in Projects."""
        with self.assertRaises(RuntimeError):
            session.BaseSession('fake hook name', 'args')


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestBaseSessionAPIProperties(TestCase):
    """Tests for the BaseSession class properties for APIs."""

    def test_gl_instance(self):
        """Returns a python-Gitlab REST API instance."""
        with mock.patch('webhook.session.get_instance') as mock_get_instance:
            mock_instance = mock.Mock(spec=['auth', 'user'])
            mock_get_instance.return_value = mock_instance
            test_session = session.BaseSession('ack_nack', 'gl_instance')

            # This is a cached property so webhook.session.get_instance should only be called once.
            self.assertIs(test_session.gl_instance, mock_instance)
            self.assertIs(test_session.gl_instance, mock_instance)
            self.assertIs(test_session.gl_instance, mock_instance)
            mock_get_instance.assert_called_once()
            # Must have called auth().
            mock_instance.auth.assert_called_once()
            # The gl_user property returns the instance user.
            self.assertEqual(test_session.gl_user, mock_instance.user)

        # Raises a RuntimeError if we're not authenticated.
        with mock.patch('webhook.session.get_instance') as mock_get_instance:
            mock_instance = mock.Mock(spec=['auth'])
            mock_get_instance.return_value = mock_instance
            with self.assertRaises(RuntimeError):
                test_session = session.BaseSession('ack_nack', 'gl_instance')
                test_session.gl_instance

    def test_graphql(self):
        """Returns a GitlabGraph instance."""
        with mock.patch('webhook.session.GitlabGraph') as mock_gitlabgraph:
            mock_instance = mock.Mock(spec=['user'])
            mock_gitlabgraph.return_value = mock_instance
            test_session = session.BaseSession('ack_nack', 'graphql')

            # This is a cached property so webhook.session.graphql should only be called once.
            self.assertIs(test_session.graphql, mock_instance)
            self.assertIs(test_session.graphql, mock_instance)
            self.assertIs(test_session.graphql, mock_instance)
            mock_gitlabgraph.assert_called_once()

        # Raises a RuntimeError if we're not authenticated.
        with mock.patch('webhook.session.GitlabGraph') as mock_gitlabgraph:
            mock_instance = mock.Mock(spec=['user'])
            mock_instance.user = None
            mock_gitlabgraph.return_value = mock_instance
            with self.assertRaises(RuntimeError):
                test_session = session.BaseSession('ack_nack', 'graphql')
                test_session.graphql

    def test_jira(self):
        """Returns a Jira instance."""
        with mock.patch('webhook.session.connect_jira') as mock_connect_jira:
            mock_instance = mock.Mock(spec=['myself'])
            mock_connect_jira.return_value = mock_instance
            test_session = session.BaseSession('ack_nack', 'jira')

            # This is a cached property so webhook.session.jira should only be called once.
            self.assertIs(test_session.jira, mock_instance)
            self.assertIs(test_session.jira, mock_instance)
            self.assertIs(test_session.jira, mock_instance)
            mock_connect_jira.assert_called_once()

        # Raises a RuntimeError if we're not authenticated.
        with mock.patch('webhook.session.connect_jira') as mock_connect_jira:
            mock_instance = mock.Mock(spec=['myself'])
            mock_instance.myself.side_effect = JIRAError('oh no!')
            mock_connect_jira.return_value = mock_instance
            with self.assertRaises(RuntimeError):
                test_session = session.BaseSession('ack_nack', 'graphql')
                test_session.jira


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
@mock.patch.dict('os.environ', {'TEST_ROUTING_KEYS': 'a.b b.c'})
class TestSessionRunner(TestCase):
    """Tests for SessionRunning init and process_* methods."""

    def test_init(self):
        """Sets up the handlers."""
        # All the possible MessageTypes and GitlabObjectKinds.
        message_types = [mtype for mtype in session.MessageType if mtype is not
                         session.MessageType.GITLAB]
        object_kinds = [okind for okind in session.GitlabObjectKind]

        # Create the input handler dict, one of each.
        mock_handlers = {}
        for member in message_types + object_kinds:
            mock_handlers[member] = mock.Mock()

        # Create a SessionRunner.
        test_session = session.SessionRunner('ack_nack', 'args', mock_handlers)

        # We should have the same number of handlers.
        self.assertEqual(len(test_session.handlers), len(mock_handlers))

        for member in message_types + object_kinds:
            # Confirm __post_init__ created Handler instances of the expected type.
            message_type = session.MessageType.GITLAB if member in object_kinds else member
            expected_class = session.HANDLER_CLASSES[message_type]
            self.assertIsInstance(test_session.get_handler(member), expected_class)

            # It is mentioned in the repr.
            self.assertIn(member.name, str(test_session))

    def test_build_cmdline_message_raises(self):
        """Raises ValueError if the --merge-request arg is not set."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        with self.assertRaises(ValueError):
            session.SessionRunner.build_cmdline_message(args)

    def test_handle_merge_request_url(self):
        """Handles a --merge-request."""
        mr_url = fake_payloads.MR_URL
        kinds = (session.GitlabObjectKind.MERGE_REQUEST,
                 session.GitlabObjectKind.NOTE,
                 session.GitlabObjectKind.PIPELINE,
                 session.GitlabObjectKind.PUSH)
        handlers = {kind: mock.Mock() for kind in kinds}
        parser = get_arg_parser('TEST')

        tests = [(f'--merge-request {mr_url}', session.GitlabObjectKind.MERGE_REQUEST),
                 (f'--merge-request {mr_url} --note hello', session.GitlabObjectKind.NOTE),
                 (f'--merge-request {mr_url} --action pipeline', session.GitlabObjectKind.PIPELINE),
                 (f'--merge-request {mr_url} --action push', session.GitlabObjectKind.PUSH)
                 ]

        for cmdline, expected_gl_kind in tests:
            with self.subTest(cmdline=cmdline, expected_gl_kind=expected_gl_kind):
                args = parser.parse_args(cmdline.split())

                with mock.patch.object(session.SessionRunner, 'process_one_message') as \
                        mock_process_one_message:
                    test_session = session.SessionRunner('buglinker', args, handlers)
                    result = test_session.run()

                self.assertEqual(result, mock_process_one_message.return_value)

                gitlab_msg = session.SessionRunner.build_cmdline_message(args)
                self.assertIs(gitlab_msg.object_kind, expected_gl_kind)

                mock_process_one_message.assert_called_once_with(
                    routing_key='cmdline',
                    headers={'message-type': 'gitlab'},
                    body=gitlab_msg.as_payload()
                )

    @mock.patch.object(session.SessionRunner, 'process_one_message')
    def test_handle_process_json(self, mock_process_one_message):
        """Handles a --json-message-file."""
        filename = 'jason_file.json'
        handlers = {kind: mock.Mock() for kind in session.GitlabObjectKind}
        parser = get_arg_parser('TEST')
        args = parser.parse_args(f'--json-message-file={filename}'.split())

        payloads = [fake_payloads.MR_PAYLOAD,
                    fake_payloads.NOTE_PAYLOAD,
                    fake_payloads.BUILD_PAYLOAD,
                    fake_payloads.PIPELINE_PAYLOAD,
                    fake_payloads.PUSH_PAYLOAD]

        for count, payload in enumerate(payloads):
            mock_process_one_message.reset_mock()
            with self.subTest(payloads_index=count):
                # Create the Session before we patch pathlib.Path because __post_init__ uses
                # that library to read the rh_metadata.yaml.
                test_session = session.SessionRunner('bughook', args, handlers)

                with mock.patch.object(session.Path, 'read_text') as mock_read_text:
                    mock_read_text.return_value = json.dumps(payload)
                    result = test_session.run()

                self.assertEqual(result, mock_process_one_message.return_value)
                mock_process_one_message.assert_called_once_with(
                    routing_key=filename,
                    headers={'message-type': 'gitlab'},
                    body=json.loads(mock_read_text.return_value)
                )

    @mock.patch.object(session.GitlabHandler, 'should_process_message', mock.Mock())
    def test_process_one_message_no_handler(self):
        """Returns False when there is no registered Handler for the given message type."""
        handlers = {session.GitlabObjectKind.NOTE: mock.Mock()}
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])

        routing_key = 'kwf.gitlab.cki-project.kernel-ark.merge-request'
        headers = {'message-type': 'gitlab'}
        body = {'object_kind': 'merge_request'}

        test_session = session.SessionRunner('bughook', args, handlers)
        result = test_session.process_one_message(routing_key, headers, body)

        self.assertFalse(result)

        gitlab_handler_note = test_session.handlers[session.GitlabObjectKind.NOTE]
        gitlab_handler_note.should_process_message.assert_not_called()
        gitlab_handler_note.handler_func.assert_not_called()

    @mock.patch.object(session.GitlabHandler, 'should_process_message',
                       mock.Mock(return_value=False))
    def test_process_one_message_should_not(self):
        """Returns False because the Handler.should_process_message method returns False."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])

        routing_key = 'kwf.gitlab.cki-project.kernel-ark.merge-request'
        headers = {'message-type': 'gitlab'}
        body = {'object_kind': 'note'}

        handlers = {session.GitlabObjectKind.NOTE: mock.Mock(name='note_func')}
        test_session = session.SessionRunner('bughook', args, handlers)

        result = test_session.process_one_message(routing_key, headers, body)
        self.assertFalse(result)

        gitlab_handler_note = test_session.handlers[session.GitlabObjectKind.NOTE]
        gitlab_handler_note.should_process_message.assert_called_once_with(headers, body)
        gitlab_handler_note.handler_func.assert_not_called()

    @mock.patch.object(session.AmqpHandler, 'should_process_message', mock.Mock(return_value=True))
    def test_process_one_message_should_do(self):
        """Returns True and calls handler_func."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])

        routing_key = 'kwf.amqp-bridge'
        headers = {'message-type': session.MessageType.AMQP.value}
        body = {'hello': 'there'}

        handlers = {session.MessageType.AMQP: mock.Mock(name='amqp_func')}
        test_session = session.SessionRunner('bughook', args, handlers)

        result = test_session.process_one_message(routing_key, headers, body)
        self.assertTrue(result)

        gitlab_handler_amqp = test_session.handlers[session.MessageType.AMQP]
        gitlab_handler_amqp.should_process_message.assert_called_once_with(headers, body)
        gitlab_handler_amqp.handler_func.assert_called_once_with(body, test_session)

    def test_queue(self):
        """Returns a MessageQueue instance."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        handlers = {session.MessageType.AMQP: mock.Mock(name='amqp_func')}

        with mock.patch('webhook.session.MessageQueue') as mock_messagequeue:
            mock_instance = mock.Mock()
            mock_messagequeue.return_value = mock_instance
            test_session = session.SessionRunner('ack_nack', args, handlers)

            # This is a cached property so webhook.session.MessageQueue should only be called once.
            self.assertIs(test_session.queue, mock_instance)
            self.assertIs(test_session.queue, mock_instance)
            self.assertIs(test_session.queue, mock_instance)
            mock_messagequeue.assert_called_once()

            test_session.consume_messages()
            mock_instance.consume_messages.assert_called_once()


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestSessionRunnerRun(TestCase):
    """Tests for the run() method."""

    def test_run_with_merge_request(self):
        """Calls the process_cmdline_message function."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args('--merge-request https://test.url.string.biz'.split())
        with mock.patch.object(session.SessionRunner, 'process_cmdline_message') as mock_process:
            test_session = session.SessionRunner('bughook', args, {})

            result = test_session.run()
            self.assertIs(result, mock_process.return_value)
            mock_process.assert_called_once()

    def test_run_with_json_message_file(self):
        """Calls the process_json_message function."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args('--json-message-file /home/user/event.json`'.split())
        with mock.patch.object(session.SessionRunner, 'process_json_message') as mock_process:
            test_session = session.SessionRunner('bughook', args, {})

            result = test_session.run()
            self.assertIs(result, mock_process.return_value)
            mock_process.assert_called_once()

    def test_run_consume_messages(self):
        """Calls the consume_messages function."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args('--rabbitmq-routing-key a.b.c'.split())
        with mock.patch.object(session.SessionRunner, 'consume_messages') as mock_consume:
            test_session = session.SessionRunner('ckihook', args, {})

            result = test_session.run()
            self.assertIs(result, mock_consume.return_value)
            mock_consume.assert_called_once()

    def test_run_consume_messages_raises(self):
        """Raises a RuntimeError when --rabbitmq-routing-key is not set."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])

        test_session = session.SessionRunner('ckihook', args, {})
        with self.assertRaises(RuntimeError):
            test_session.run()


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestGitlabHandler(TestCase):
    """Tests for the GitlabHandler class."""

    @mock.patch.object(session.BaseSession, 'gl_instance', mock.Mock())
    @mock.patch.object(session.BaseSession, 'graphql', mock.Mock())
    @mock.patch.object(session.BaseSession, 'jira', mock.Mock())
    def create_session(self, hook_name, cmdline=None, gl_instance=None,
                       graphql=None, jira=None, handlers=None):
        """Return a new Session instance with the given params."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args(cmdline or [])
        if handlers:
            test_session = session.SessionRunner(hook_name, args, handlers)
        else:
            test_session = session.BaseSession(hook_name, args)

        test_session.gl_instance = gl_instance or fakes.FakeGitLab()
        test_session.graphql = graphql or mock.Mock()
        test_session.jira = jira or mock.Mock()

        test_session.gl_instance.auth()
        return test_session

    def run_handler_tests(self, test_list, test_session=None):
        """Run the tests on the given handler."""
        # Each test in the list is a a list with three items:
        # [0] = expected result
        # [1] = object_kind
        # [2] = body

        if not test_session:
            test_session = self.create_session('ack_nack')
        headers = {}

        for expected_result, object_kind, body in test_list:
            handler_func = mock.Mock(name=object_kind.name)
            with self.subTest(expected_result=expected_result, kind=object_kind, body=body):
                test_handler = session.GitlabHandler(test_session, handler_func, object_kind)
                result = test_handler.should_process_message(headers, body)
                self.assertEqual(expected_result, result)
                self.assertIn(object_kind.name, str(test_handler))

    @staticmethod
    def set_payload_project_id_namespace(body, project_id=None, namespace=None):
        """Do what it says."""
        if project_id:
            if variables := get_nested_key(body, 'object_attributes/variables'):
                for var in variables:
                    if var['key'] == 'mr_project_id':
                        var['value'] = project_id
            elif body['object_kind'] == 'build':
                body['project_id'] = project_id
            else:
                body['project']['id'] = project_id
        if namespace and 'project' in body:
            body['project']['path_with_namespace'] = namespace

    def test_should_process_message_no_project(self):
        """Returns False because these payloads have no matching project in the yaml."""
        tests = [
            [False, session.GitlabObjectKind.PIPELINE,
             deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)],
            [False, session.GitlabObjectKind.MERGE_REQUEST, deepcopy(fake_payloads.MR_PAYLOAD)],
            [False, session.GitlabObjectKind.NOTE, deepcopy(fake_payloads.NOTE_PAYLOAD)],
            [False, session.GitlabObjectKind.PIPELINE, deepcopy(fake_payloads.PIPELINE_PAYLOAD)],
        ]
        self.run_handler_tests(tests)

    def test_should_process_message_good_result(self):
        """Returns True because the user, project, environment, and hook are all okay."""
        tests = [
            [True, session.GitlabObjectKind.PIPELINE,
             deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)],
            [True, session.GitlabObjectKind.MERGE_REQUEST, deepcopy(fake_payloads.MR_PAYLOAD)],
            [True, session.GitlabObjectKind.NOTE, deepcopy(fake_payloads.NOTE_PAYLOAD)],
            [True, session.GitlabObjectKind.PIPELINE, deepcopy(fake_payloads.PIPELINE_PAYLOAD)],
            [True, session.GitlabObjectKind.BUILD, deepcopy(fake_payloads.BUILD_PAYLOAD)],
        ]

        # Fix up the payload data.
        project_id = 11223344
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        for test in tests:
            body = test[2]
            # Force the payload project ID to be one that exists in rh_metadata.yaml.
            self.set_payload_project_id_namespace(body, project_id, namespace)

        self.run_handler_tests(tests)

    def test_should_process_message_bot_user(self):
        """Returns False due to event from same user we are logged in as (bot)."""
        # Or, for build & pipelines we don't check the event user so these should pass.
        tests = [
            [True, session.GitlabObjectKind.PIPELINE,
             deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)],
            [False, session.GitlabObjectKind.MERGE_REQUEST, deepcopy(fake_payloads.MR_PAYLOAD)],
            [False, session.GitlabObjectKind.NOTE, deepcopy(fake_payloads.NOTE_PAYLOAD)],
            [True, session.GitlabObjectKind.PIPELINE, deepcopy(fake_payloads.PIPELINE_PAYLOAD)],
            [True, session.GitlabObjectKind.BUILD, deepcopy(fake_payloads.BUILD_PAYLOAD)],
        ]
        user_dict = deepcopy(fakes.AUTH_USER)
        user_dict['email'] = user_dict.pop('public_email')
        for test in tests:
            body = test[2]
            body['user'] = user_dict

        # Fix up the payload data.
        user_dict = deepcopy(fakes.AUTH_USER)
        user_dict['email'] = user_dict.pop('public_email')
        project_id = 11223344
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        for test in tests:
            body = test[2]
            # Force the payload project ID to be one that exists in rh_metadata.yaml.
            self.set_payload_project_id_namespace(body, project_id, namespace)
            # Set the payload user to be the same as we're logged in as.
            body['user'] = user_dict

        self.run_handler_tests(tests)

    def test_should_process_message_staging_non_sandbox(self):
        """Returns False as the Project is not sandbox but we are in a Staging env."""
        tests = [
            [False, session.GitlabObjectKind.PIPELINE,
             deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)],
            [False, session.GitlabObjectKind.MERGE_REQUEST, deepcopy(fake_payloads.MR_PAYLOAD)],
            [False, session.GitlabObjectKind.NOTE, deepcopy(fake_payloads.NOTE_PAYLOAD)],
            [False, session.GitlabObjectKind.PIPELINE, deepcopy(fake_payloads.PIPELINE_PAYLOAD)],
            [False, session.GitlabObjectKind.BUILD, deepcopy(fake_payloads.BUILD_PAYLOAD)],
        ]

        # Fix up the payload data.
        project_id = 11223344
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        for test in tests:
            body = test[2]
            # Force the payload project ID to be one that exists in rh_metadata.yaml.
            self.set_payload_project_id_namespace(body, project_id, namespace)

        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'staging'}):
            self.run_handler_tests(tests)

    def test_should_process_message_production_sandbox(self):
        """Returns False as the Project is sandbox and we are in a Production env."""
        tests = [
            [False, session.GitlabObjectKind.PIPELINE,
             deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)],
            [False, session.GitlabObjectKind.MERGE_REQUEST, deepcopy(fake_payloads.MR_PAYLOAD)],
            [False, session.GitlabObjectKind.NOTE, deepcopy(fake_payloads.NOTE_PAYLOAD)],
            [False, session.GitlabObjectKind.PIPELINE, deepcopy(fake_payloads.PIPELINE_PAYLOAD)],
            [False, session.GitlabObjectKind.BUILD, deepcopy(fake_payloads.BUILD_PAYLOAD)],
        ]

        # Fix up the payload data.
        project_id = 56789
        namespace = 'redhat/rhel/src/kernel/rhel-8-sandbox'
        for test in tests:
            body = test[2]
            # Force the payload project ID to be one that exists in rh_metadata.yaml,
            # this time a sandbox one!
            self.set_payload_project_id_namespace(body, project_id, namespace)

        with mock.patch.dict('os.environ', {'CKI_DEPLOYMENT_ENVIRONMENT': 'production'}):
            self.run_handler_tests(tests)

    def test_should_process_message_no_project_matching(self):
        """Ignores mismatched projects if match_to_projects is False."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args('--disable-user-check'.split())
        headers = {}
        body = {'project': {'path_with_namespace': 'some/unknown/project'}}

        tests = [
            # (expected_result, webhook_name)
            (False, 'ack_nack'),
            (False, 'bughook'),
            (False, 'buglinker'),
            (False, 'ckihook'),
            (False, 'mergehook'),
            (True, 'sprinter'),
            (True, 'terminator'),
            (False, 'umb_bridge'),
        ]

        for expected_result, webhook_name in tests:
            test_session = session.BaseSession(webhook_name, args)
            handler_func = mock.Mock(name=f'handler func for {webhook_name}')
            test_handler = session.GitlabHandler(
                test_session, handler_func, session.GitlabObjectKind.NOTE
            )
            test_handler.filter_message = mock.Mock(return_value=True)
            with self.subTest(expected_result=expected_result, webhook_name=webhook_name):
                with self.assertLogs('cki.webhook.session', level='INFO') as logs:
                    result = test_handler.should_process_message(headers, body)
                    self.assertEqual(expected_result, result)
                    # Try to confirm it did it for the right reason.
                    logging_assertion = self.assertIn if expected_result else self.assertNotIn
                    logging_assertion('Skipping Project check for this webhook',
                                      '\n'.join(logs.output))

    def test_should_process_message_unexpected_webhook(self):
        """Returns False as the current webhook is not expected on this Project."""
        tests = [
            [False, session.GitlabObjectKind.PIPELINE,
             deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)],
            [False, session.GitlabObjectKind.MERGE_REQUEST, deepcopy(fake_payloads.MR_PAYLOAD)],
            [False, session.GitlabObjectKind.NOTE, deepcopy(fake_payloads.NOTE_PAYLOAD)],
            [False, session.GitlabObjectKind.PIPELINE, deepcopy(fake_payloads.PIPELINE_PAYLOAD)],
            [False, session.GitlabObjectKind.BUILD, deepcopy(fake_payloads.BUILD_PAYLOAD)],
        ]

        # Fix up the payload data.
        project_id = 11223344
        namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        for test in tests:
            body = test[2]
            self.set_payload_project_id_namespace(body, project_id, namespace)

        # Create our own session so we can rip out one of the Project webhooks 😬.
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        test_session = session.BaseSession('ckihook', args)
        test_session.gl_instance = fakes.FakeGitLab()
        test_session.gl_instance.auth()
        del test_session.rh_projects.projects[11223344].webhooks['ckihook']

        self.run_handler_tests(tests, test_session=test_session)

    def test_event_project_build(self):
        """Gets the upstream project_id of a downstream build (job) event."""
        # Mock up the payload.
        upstream_namespace = 'redhat/centos-stream/src/kernel/centos-stream-9'
        body = deepcopy(fake_payloads.BUILD_PAYLOAD)
        body['source_pipeline'] = {'project': {'path_with_namespace': upstream_namespace}}

        test = [True, session.GitlabObjectKind.BUILD, body]
        self.run_handler_tests([test])

    def test_filter_messages_bad_note(self):
        """Returns False because a note event is of the wrong type."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        test_session = session.BaseSession('ckihook', args)

        test_gl_handler = session.GitlabHandler(test_session, mock.Mock(),
                                                session.GitlabObjectKind.NOTE)

        headers = {}
        body = deepcopy(fake_payloads.NOTE_PAYLOAD)
        body['object_attributes']['noteable_type'] = 'Issue'

        result = test_gl_handler.filter_message(headers, body)
        self.assertFalse(result)

    def test_filter_messages_closed_mr(self):
        """Returns False because the MR is closed and the action is not 'close'."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        test_session = session.BaseSession('buglinker', args)

        test_gl_handler = session.GitlabHandler(test_session, mock.Mock(),
                                                session.GitlabObjectKind.MERGE_REQUEST)

        headers = {}
        body = deepcopy(fake_payloads.MR_PAYLOAD)
        body['object_attributes']['state'] = 'closed'
        body['object_attributes']['action'] = 'update'

        result = test_gl_handler.filter_message(headers, body)
        self.assertFalse(result)


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
@mock.patch.dict('os.environ', {'BUGZILLA_EMAIL': 'bot@example.com'})
class TestAmqpHandler(TestCase):
    """Tests for the AmqpHandler."""

    def test_processing_amqp_message(self):
        """Returns True when the event should be processed."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        test_session = session.BaseSession('umb_bridge', args)

        test_amqp_handler = session.AmqpHandler(test_session, mock.Mock())

        headers = {}  # Currently we do not look at amqp message headers beyond matchingt the type.

        tests = [
            # Non-bot user, has bug_id: True
            (True, {'event': {'user': {'login': 'user@example.com'}, 'bug_id': 2356427}}),
            # Bot user (matches BUGZILLA_EMAIL env): False
            (False, {'event': {'user': {'login': 'bot@example.com'}, 'bug_id': 2356427}}),
            # No bug_id: False
            (False, {'event': {'user': {'login': 'user@example.com'}}}),
        ]

        for expected_result, body in tests:
            with self.subTest(expected_result=expected_result, body=body):
                self.assertIs(
                    expected_result,
                    test_amqp_handler.should_process_message(headers, body)
                )


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestJiraHandler(TestCase):
    """Tests for the Jira event handler."""

    def test_processing_jira_message(self):
        """Returns True when the event should be processed."""
        parser = get_arg_parser('TEST')
        args = parser.parse_args([])
        test_session = session.BaseSession('bughook', args)
        test_session.jira = mock.Mock()

        test_jira_handler = session.JiraHandler(test_session, mock.Mock())

        headers = {}  # We don't look at the jira event headers other than to match message type.

        valid_bot_user = session.JIRA_BOT_ACCOUNTS[0]
        linked_url = 'https://gitlab.com/redhat/rhel/src/kernel/rhel-8/-/merge_requests/4824'

        tests = [
            # Non-bot user, issue is in the RHEL space, linked to an MR in known project: True
            (True, {'issue': {'key': 'RHEL-12345'}, 'user': {'name': 'jira_user'}}),
            # Bot user: False
            (False, {'issue': {'key': 'RHEL-12345'}, 'user': {'name': valid_bot_user}}),
            # Not in the RHEL space: False
            (False, {'issue': {'key': 'RHELPLAN-5345'}, 'user': {'name': 'jira_user'}}),
        ]

        for expected_result, body in tests:
            with self.subTest(expected_result=expected_result, body=body):
                with mock.patch('webhook.session.get_linked_mrs') as mock_get_linked_mrs:
                    mock_get_linked_mrs.return_value = [linked_url]
                    self.assertIs(
                        expected_result,
                        test_jira_handler.should_process_message(headers, body)
                    )


@mock.patch.dict('os.environ', {'RH_METADATA_YAML_PATH': 'tests/fake_rh_metadata.yaml'})
class TestUmbBridgeHandler(TestCase):
    """Tests for the UmbBridgeHandler class."""

    def test_processing_umb_bridge_message(self):
        """Returns True if the message should be processed."""
        class BridgeTest(NamedTuple):
            """Values to test with."""
            session: str
            headers: dict
            body: dict
            expected_result: bool

        # Generate a set of tests for each relevant webhook.
        webhooks_that_use_umb_bridge = ('bughook', 'jirahook')
        for webhook_name in webhooks_that_use_umb_bridge:
            args = get_arg_parser('TEST').parse_args([])
            test_session = session.BaseSession(webhook_name, args)
            # All the namespaces where this webhook is active.
            namespaces = [project.namespace for project in
                          test_session.rh_projects.projects.values() if
                          webhook_name in project.webhooks]
            # If there are no projects in the yaml where the hook is active then we can't test!
            self.assertTrue(namespaces)

            tests = []
            for namespace in namespaces:
                tests.append(
                    # Project is known and source header key matches: passes
                    BridgeTest(
                        session=test_session,
                        headers={'event_target_webhook': webhook_name},
                        body={'mrpath': f'{namespace}!123'},
                        expected_result=True
                    )
                )
                tests.append(
                    # Header event_target_webhook does not match session webhook: fails
                    BridgeTest(
                        session=test_session,
                        headers={'event_target_webhook': 'signoff'},
                        body={'mrpath': f'{namespace}!123'},
                        expected_result=False
                    )
                )

            tests.append(
                # Event body's mrpath namespace is not in the yaml: fails
                BridgeTest(
                    session=test_session,
                    headers={'event_target_webhook': webhook_name},
                    body={'mrpath': 'some/unknown/namespace!123'},
                    expected_result=False
                )
            )

        for test in tests:
            with self.subTest(**test._asdict()):
                test_umb_bridge_handler = session.UmbBridgeHandler(test.session, mock.Mock())
                result = test_umb_bridge_handler.should_process_message(test.headers, test.body)
                self.assertIs(test.expected_result, result)
