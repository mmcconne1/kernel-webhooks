"""Tests for the description library."""
from unittest import TestCase
from unittest import mock

from tests.fakes_bz import FakeBZ
from webhook import defs
from webhook import description


class TestMRDescription(TestCase):
    """Tests for the MRDescription dataclass."""

    text = ('Hello\n'
            'THINGS THAT SHOULD MATCH:\n'
            'Bugzilla: https://bugzilla.redhat.com/123456  \n'                         # BZ 123456
            'Bugzilla: http://bugzilla.redhat.com/show_bug.cgi?id=637382\n'            # BZ 637382
            'CVE: CVE-2021-61677\n'                                               # CVE-2021-61677
            'CVE: CVE-1992-15616\n'                                               # CVE-1992-15616
            'Depends: https://bugzilla.redhat.com/262727  \n'                         # Dep 262727
            'Signed-off-by: User Name <user@example.com> \n'  # DCO 'User Name', 'user@example.com'
            'Depends: !737\n'                                                          # Dep !737
            'Cc: <noname@example.com>   \n'
            f'Depends: {defs.GITFORGE}/group/project/-/merge_requests/267\n'           # Dep !267
            'Signed-off-by: Artist <artist@example.com>\n'    # DCO 'Artist', 'artist@example.com'
            'hey\nDepends: http://gitlab.com/group1/project/-/merge_requests/321  \n'  # Dep !321
            'THINGS THAT SHOULD NOT MATCH:\n'
            'Cc: Example User <example_user@redhat.com>  \n'
            'Bugzilla: 34567\nBugzilla: BZ-456789\n'
            'Cc: Example <example@fedoraproject.org>\n'
            'Cc: Someone Else <someone_else@example.com>\n'
            'Signed-off-by: example <example@example.com> 😎\n'
            '    Bugzilla: https://bugzilla.redhat.com/23456\n'
            '    Signed-off-by: Person <person@example.com>\n'
            )

    def test_MRDescription_empty(self):
        """Returns as False"""
        test_desc = description.MRDescription(text='')
        self.assertIs(bool(test_desc), False)
        self.assertEqual(test_desc.bugzilla, set())
        self.assertEqual(test_desc.cc, set())
        self.assertEqual(test_desc.cve, set())
        self.assertEqual(test_desc.depends, set())
        self.assertEqual(test_desc.depends_mrs, set())
        self.assertIs(test_desc.marked_internal, False)

    @mock.patch('webhook.description.resolve_bzs_to_mrs', mock.Mock(return_value=set()))
    def test_MRDescription_equal(self):
        """Evaluates as equal when the text, namespace, and _depends match."""
        namespace = 'space/project'
        desc1 = description.MRDescription(text=self.text, namespace=namespace)
        desc1._depends_bzs.update([101, 202])
        desc2 = description.MRDescription(text=self.text, namespace=namespace)
        self.assertNotEqual(desc1, desc2)
        desc2._depends_bzs.update([101, 202])
        self.assertEqual(desc1, desc2)

    def test_MRDescription_bugzilla(self):
        """Parses the Bugzilla: tags."""
        test_desc = description.MRDescription(text=self.text)
        self.assertEqual(test_desc.bugzilla, {123456, 637382})

    def test_MRDescription_cc(self):
        """Parses the Cc: tags."""
        test_desc = description.MRDescription(text=self.text)
        self.assertEqual(test_desc.cc, {('Example', 'example@fedoraproject.org'),
                                        ('Someone Else', 'someone_else@example.com'),
                                        ('Example User', 'example_user@redhat.com'),
                                        ('', 'noname@example.com')})

    def test_MRDescription_cve(self):
        """Parses the CVE: tags."""
        test_desc = description.MRDescription(text=self.text)
        self.assertEqual(test_desc.cve, {'CVE-2021-61677', 'CVE-1992-15616'})

    def test_MRDescription_depends(self):
        """Parses the Depends: tags for BZ IDs."""
        test_desc = description.MRDescription(text=self.text)
        self.assertEqual(test_desc.depends, {262727})

    def test_MRDescription_depends_with_depends(self):
        """Parses the Depends: tags for BZ IDs and appends _depends."""
        test_desc = description.MRDescription(text=self.text)
        test_desc._depends_bzs.update([123, 456])
        self.assertEqual(test_desc.depends, {123, 262727, 456})

    def test_MRDescription_depends_mrs_no_namespace(self):
        """Parses the Depends: tags for MR IDs."""
        test_desc = description.MRDescription(text=self.text)
        self.assertEqual(test_desc.depends_mrs, {737})

    @mock.patch('webhook.description.resolve_bzs_to_mrs', mock.Mock(return_value=set()))
    def test_MRDescription_depends_mrs_with_namespace_no_fetch_bugs(self):
        """Parses the Depends: tags for MR IDs."""
        namespace = 'group1/project'
        test_desc = description.MRDescription(text=self.text, namespace=namespace)
        self.assertEqual(test_desc.depends_mrs, {321, 737})

    @mock.patch('webhook.libbz.fetch_bugs')
    def test_MRDescription_depends_mrs_with_namespace(self, mock_fetch_bugs):
        """Parses the Depends: tags for MR IDs and get MR IDs from bugzilla."""
        namespace = 'group/project'
        et_data1 = {'ext_bz_bug_id': 'group1/project/-/merge_requests/555',
                    'type': {'url': defs.EXT_TYPE_URL}}
        et_data2 = {'ext_bz_bug_id': f'{namespace}/-/merge_requests/999',
                    'type': {'url': defs.EXT_TYPE_URL}}
        et_data3 = {'ext_bz_bug_id': f'{namespace}/-/merge_requests/526',
                    'type': {'url': defs.EXT_TYPE_URL}}
        fake_bug = FakeBZ(id=262727, external_bugs=[et_data1, et_data2, et_data3])
        mock_fetch_bugs.return_value = [fake_bug]
        test_desc = description.MRDescription(text=self.text, namespace=namespace)
        self.assertEqual(test_desc.depends_mrs, {267, 526, 737, 999})

    def test_MRDescription_marked_internal(self):
        """Returns True if Bugzilla: INTERNAL is found, otherwise False."""
        test_desc = description.MRDescription(text=self.text)
        self.assertIs(test_desc.marked_internal, False)

        text = self.text + '\nBugzilla: INTERNAL  \n'
        test_desc = description.MRDescription(text=text)
        self.assertIs(test_desc.marked_internal, True)

    def test_MRDescription_signoff(self):
        """Parses the Signed-off-by: tags for name/email tuples."""
        test_desc = description.MRDescription(text=self.text)
        self.assertEqual(test_desc.signoff, {('User Name', 'user@example.com'),
                                             ('Artist', 'artist@example.com')})

    @mock.patch('webhook.description.resolve_bzs_to_mrs', mock.Mock(return_value=set()))
    def test_MRDescription_depends_mrs_with_graphql(self):
        """Parses the Depends tag for MR IDs and gets BZ IDs from dependant MRs."""
        mock_graph = mock.Mock()

        mr267_text = ('Bugzilla: https://bugzilla.redhat.com/5432543\n'
                      'Depends: https://bugzilla.redhat.com/2675423')
        mr737_text = ('Bugzilla: https://bugzilla.redhat.com/3254325\n'
                      'Depends: https://bugzilla.redhat.com/2675423')
        mock_graph.get_mr_descriptions.return_value = {267: mr267_text, 737: mr737_text}
        namespace = 'group/project'
        test_desc = description.MRDescription(self.text, namespace, mock_graph)
        self.assertEqual(test_desc.depends_mrs, {267, 737})


class TestCommit(TestCase):
    """Tests for the Commit dataclass."""

    def test_commit_empty(self):
        """Returns an empty Commit with all default values."""
        commit = description.Commit()
        self.assertIs(commit.author, None)
        self.assertEqual(commit.authorEmail, '')
        self.assertEqual(commit.authorName, '')
        self.assertIs(commit.date, None)
        self.assertEqual(commit.description, description.Description())
        self.assertEqual(commit.sha, '')
        self.assertEqual(commit.short_sha, '')
        self.assertEqual(commit.title, '')
        self.assertIs(commit.expected_signoff, None)
        self.assertIn("Commit  ('')", str(commit))

    def test_commit_from_dict(self):
        """Returns a Commit populated from the input dict."""
        input_dict = {'author': {'username': 'test_user',
                                 'name': 'Test User',
                                 'gid': 'gid://Gitlab/User/1234567'},
                      'authorEmail': 'test_user@example.com',
                      'authorName': 'Test User',
                      'authoredDate': '2022-12-06T11:59:50Z',
                      'description': 'a commit',
                      'sha': 'd5de301285a720250ef97568f5dbdcbbe8480d5b',
                      'title': 'an example commit'
                      }
        commit = description.Commit(input_dict=input_dict)
        self.assertEqual(commit.author.username, 'test_user')
        self.assertEqual(commit.authorEmail, 'test_user@example.com')
        self.assertEqual(commit.authorName, 'Test User')
        self.assertTrue(isinstance(commit.date, description.datetime))
        self.assertEqual(commit.description.text, 'a commit')
        self.assertEqual(commit.sha, 'd5de301285a720250ef97568f5dbdcbbe8480d5b')
        self.assertEqual(commit.short_sha, 'd5de301285a7')
        self.assertEqual(commit.title, 'an example commit')
        self.assertEqual(commit.expected_signoff, ('Test User', 'test_user@example.com'))


class TestCommitDCOState(TestCase):
    """Test the Commit.dco_state functionality."""

    def test_dco_state(self):
        """Returns the expected DCOState for the given Commit."""
        tests = [
            # (expected_state, author(name, email), signoff(name, email))
            # Returns NOT_REDHAT state because the author is not redhat.com.
            (defs.DCOState.OK_NOT_REDHAT,
             ('User Example', 'user@example.com'), ('User Example', 'user@example.com')
             ),
            # Returns OK because the author has signed off and is from redhat.
            (defs.DCOState.OK,
             ('Red Hatter', 'user@redhat.com'), ('Red Hatter', 'user@redhat.com')
             ),
            # Returns OK state because the expected signoff is in the Description.
            (defs.DCOState.OK,
             ('Example P. User', 'example@redhat.com'), ('Example P. User', 'example@redhat.com')
             ),
            (defs.DCOState.MISSING, ('', 'example@redhat.com'), ('', '')),
            # Returns UNRECOGNIZED state because the signoffs are not related at all.
            (defs.DCOState.UNRECOGNIZED,
             ('Example User', 'example@redhat.com'), ('A User', 'a_user@redhat.com')
             ),
            # Returns NAME_MATCHES state because a signoff was found with a matching name.
            (defs.DCOState.NAME_MATCHES,
             ('Example User', 'example@redhat.com'), ('Example User', 'a_user@redhat.com')
             ),
            # Returns EMAIL_MATCHES state because a signoff was found with a matching email.
            (defs.DCOState.EMAIL_MATCHES,
             ('Example User', 'example@redhat.com'), ('Example User 2', 'example@redhat.com')
             ),
            # Uses unidecode so differences due to accents etc are ignored.
            (defs.DCOState.OK,
             ('Examplé User', 'example@redhat.com'), ('Example User', 'example@redhat.com')
             ),
        ]

        for expected_state, author, signoff in tests:
            signoff_str = f'Signed-off-by: {signoff[0]} <{signoff[1]}>'
            with self.subTest(expected_state=expected_state, author_tuple=author,
                              signoff_str=signoff_str):
                test_description = description.Description(signoff_str)
                test_commit = description.Commit(authorName=author[0], authorEmail=author[1],
                                                 description=test_description)
                self.assertIs(expected_state, test_commit.dco_state)
