# Upstream commit ID comparison Webhook

## Purpose

This webhook compares backported commits against their upstream source, and
identifies RHEL specific issues such as KABI changes.

## Reporting

- Label prefix: `CommitRefs::`
- Comment header: **Upstream Commit ID Readiness Report**

The webhook reports the overall result of the check by applying a scoped label
to the MR with the prefix `CommitRefs::`.

The hook will leave a comment on the MR with the details of the check. The
header of the comment is **Upstream Commit ID Readiness Report**. This comment will
be edited with updated information every time the hook runs. Refer to the timestamp
at the end of the comment to see when the hook last evaluated the MR.

## Triggering

To trigger reevaluation of an MR by the commit-compare webhook either remove any
existing `CommitRefs::` scoped label or a leave a comment with one of the following:

- `request-commit-evaluation`
- `request-commit-id-evaluation`
- `request-commit-compare-evaluation`

Alternatively, to retrigger all the webhooks at the same time leave a note in
the MR with `request-evaluation`.

## Manual runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.commit_compare \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
        --linux-src /usr/src/linux

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

The `--linux-src` argument can also be passed to the script via the `LINUX_SRC`
environment variable.
