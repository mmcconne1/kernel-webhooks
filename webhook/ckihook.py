"""Manage the CKI labels."""
from dataclasses import dataclass
import fnmatch
from os import environ
import sys

from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import get_variables
from jinja2 import Environment
from jinja2 import FileSystemLoader
import prometheus_client as prometheus

from webhook import common
from webhook.base_mr import BaseMR
from webhook.base_mr_mixins import PipelinesMixin
from webhook.defs import GitlabObjectKind
from webhook.defs import MrState
from webhook.pipelines import CKI_LABEL_PREFIXES
from webhook.pipelines import PipelineResult
from webhook.pipelines import PipelineStatus
from webhook.pipelines import PipelineType
from webhook.session import new_session

LOGGER = logger.get_logger('cki.webhook.ckihook')


METRIC_KWF_CKIHOOK_PIPELINES_RETRIED = prometheus.Counter(
    'kwf_ckihook_pipelines_retried',
    'Number of CKI pipelines retried by ckihook',
    ['bridge_name']
)

REPORT_HEADER = '**CKI Pipelines Status:**'
INDENT = '    '


@dataclass(repr=False)
class PipeMR(PipelinesMixin, BaseMR):
    """An MR class for pipelines."""


OPEN_MRS_QUERY = """
query mrData($namespace: ID!, $branches: [String!], $first: Boolean = true, $after: String = "") {
  project(fullPath: $namespace) {
    id @include(if: $first)
    mrs: mergeRequests(
      after: $after
      state: opened
      labels: ["CKI_RT::Warning"]
      targetBranches: $branches
    ) {
      pageInfo {
        hasNextPage
        endCursor
      }
      nodes {
        iid
        headPipeline {
          jobs {
            nodes {
              allowFailure
              id
              name
              createdAt
              pipeline {
                id
              }
              status
              downstreamPipeline {
                id
                project {
                  id
                }
                status
                stages {
                  nodes {
                    name
                    jobs {
                      nodes {
                        id
                        name
                        status
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
"""

RETRY_JOB_QUERY = """
mutation retryJob($bridge_gid: CiProcessableID!) {
  jobRetry(input: {id: $bridge_gid}) {
   job {
      id
    }
  }
}
"""


def cki_label_changed(changes):
    """Return True if any CKI label changed, or False."""
    return any(common.has_label_prefix_changed(changes, f'{prefix}::') for
               prefix in list(CKI_LABEL_PREFIXES.values()) + ['CKI'])


def retry_pipeline(graphql, mr_id, pipeline):
    """Get a gitlab instance and retry the failed pipelines."""
    LOGGER.info('Retrying downstream pipeline %s for MR %s', pipeline, mr_id)
    if misc.is_production_or_staging():
        query_params = {'bridge_gid': pipeline.bridge_gid}
        graphql.client.query(RETRY_JOB_QUERY, query_params)
        METRIC_KWF_CKIHOOK_PIPELINES_RETRIED.labels(pipeline.bridge_name).inc()


def failed_rt_mrs(graphql, namespace, branches):
    """Return a dict of namespace MRs open on the branches that failed CKI_RT with a warning."""
    query_params = {'namespace': namespace, 'branches': branches}
    result = graphql.client.query(OPEN_MRS_QUERY, query_params, paged_key='project/mrs')
    return {
        mr['iid']: misc.get_nested_key(mr, 'headPipeline/jobs/nodes')
        for mr in misc.get_nested_key(result, 'project/mrs/nodes', [])
    }


def retrigger_failed_pipelines(graphql, namespace, branch):
    """Retrigger any RT pipelines that failed on or before the MERGE stage."""
    if PipelineType.REALTIME not in branch.pipelines:
        LOGGER.info('This branch does not expect a realtime pipeline: %s', branch.pipelines)
        return
    branch_names = [branch.name, branch.name.removesuffix('-rt')]
    if not (mrs := failed_rt_mrs(graphql, namespace, branch_names)):
        LOGGER.info('No open MRs for %s on branches %s, nothing to do.', namespace, branch_names)
        return
    for mr_id, raw_bridge_jobs in mrs.items():
        pipelines = PipelineResult.prepare_pipelines(raw_bridge_jobs)
        if pipe := next((p for p in pipelines
                         if (stage := p.failed_stage) and (stage.name == 'merge')), None):
            retry_pipeline(graphql, mr_id, pipe)


def get_project_pipeline_var(gl_project, pipeline_id, key):
    """Return the value of the pipeline variable matching key, or None."""
    gl_pipeline = gl_project.pipelines.get(pipeline_id)
    return get_variables(gl_pipeline).get(key)


def get_pipeline_target_branch(gl_instance, project_id, pipeline_id):
    """Return the 'branch' pipeline variable value for the given project/pipeline."""
    gl_project = gl_instance.projects.get(project_id)
    return get_project_pipeline_var(gl_project, pipeline_id, 'branch')


def get_downstream_pipeline_branch(gl_instance, pipe_result):
    """Return the 'branch' var value if found, otherwise None."""
    ds_project_id = pipe_result.ds_project_id
    ds_pipeline_id = pipe_result.ds_pipeline_id

    if not (ds_branch := get_pipeline_target_branch(gl_instance, ds_project_id, ds_pipeline_id)):
        LOGGER.debug("Could not get 'branch' variable from downstream pipeline %s", ds_pipeline_id)
    return ds_branch


def branch_changed(payload):
    """Return True if it seems like the MR branch changed, otherwise False."""
    LOGGER.info('Checking if MR target branch matches latest pipeline target branch.')
    # If the MR doesn't have a head pipeline then we're done here.
    if not payload['object_attributes']['head_pipeline_id']:
        LOGGER.info('No head pipeline, skipping target branch check.')
        return False
    # This is the signature of an MR event when the target branch changes. Maybe?
    if 'merge_status' in payload['changes']:
        return True
    LOGGER.info("'changes' dict does not have 'merge_status' key, skipping target branch check.")
    return False


def process_possible_branch_change(session, pipe_mr, payload):
    """Confirm MR branch changed and if so, Return True and cancel/trigger pipelines."""
    if not pipe_mr.pipelines:
        LOGGER.info('MR has no pipelines, nothing to do.')
        return False
    ds_branch = get_downstream_pipeline_branch(pipe_mr.gl_instance, pipe_mr.pipelines[0])
    mr_branch = payload['object_attributes']['target_branch']
    if ds_branch and ds_branch != mr_branch:
        LOGGER.info('Target branch changed: MR %s != downstream pipeline %s, triggering...',
                    mr_branch, ds_branch)
        if not misc.is_production_or_staging():
            LOGGER.info('Not production, skipping work.')
            return True
        head_pipeline_id = pipe_mr.head_pipeline_id
        gl_mr = pipe_mr.gl_mr
        common.cancel_pipeline(pipe_mr.gl_project, head_pipeline_id)
        new_pipeline_id = common.create_mr_pipeline(gl_mr)
        new_pipeline_url = f'{pipe_mr.gl_project.web_url}/-/pipelines/{new_pipeline_id}'
        note_text = ("This MR's current target branch has changed since the last pipeline"
                     f" was triggered. The last pipeline {head_pipeline_id} has been canceled"
                     f" and a new pipeline has been triggered: {new_pipeline_url}  \n"
                     f"Last pipeline MR target branch: {ds_branch}  \n"
                     f"Current MR target branch: {mr_branch}  ")
        session.update_webhook_comment(gl_mr, note_text)
        return True
    LOGGER.info("MR current target branch '%s' matches latest pipeline.", mr_branch)
    return False


def pipeline_is_waived(pipeline, mr_labels):
    """Return True if the pipeline is currently waived, otherwise False."""
    # Only possible to waive RT and Automotive pipelines.
    waiveable = (PipelineType.REALTIME, PipelineType.AUTOMOTIVE)
    # You can't waive a pipeline that isn't failed.
    return pipeline.type in waiveable and f'{pipeline.type.prefix}::Waived' in mr_labels and \
        pipeline.status is PipelineStatus.FAILED


def context_status_dict(pipelines, mr_labels):
    """Return a dict of failed, canceled, running, success, and waived PipelineResults."""
    failed = [pipe for pipe in pipelines if not pipeline_is_waived(pipe, mr_labels) and
              pipe.status is PipelineStatus.FAILED]
    canceled = [pipe for pipe in pipelines if not pipeline_is_waived(pipe, mr_labels) and
                pipe.status is PipelineStatus.CANCELED]
    running = [pipe for pipe in pipelines if pipe.status is PipelineStatus.RUNNING]
    success = [pipe for pipe in pipelines if pipe.status is PipelineStatus.SUCCESS]
    waived = [pipe for pipe in pipelines if pipeline_is_waived(pipe, mr_labels)]
    return {'failed': failed, 'canceled': canceled, 'running': running, 'success': success,
            'waived': waived}


def context_dict(pipe_mr, pipelines, header_str, invalid_branch, protected_branches):
    """Return a dict with PipelineResult data ready for a jinja template."""
    context = {
        'header_str': header_str,
        'invalid_branch': invalid_branch,
        'protected_branches': protected_branches,
        'blocking': {},
        'nonblocking': {},
        'missing': [],
        'dw_url': environ.get('DATAWAREHOUSE_URL'),
        'is_draft': pipe_mr.draft,
    }

    blocking = [pipe for pipe in pipelines.values() if pipe and not pipe.allow_failure]
    nonblocking = [pipe for pipe in pipelines.values() if pipe and pipe.allow_failure]

    context['blocking'] = context_status_dict(blocking, pipe_mr.labels)
    context['nonblocking'] = context_status_dict(nonblocking, pipe_mr.labels)
    context['missing'] = [ptype for ptype, pipeline in pipelines.items() if pipeline is None]
    return context


def generate_comment(pipe_mr, pipelines, cki_status_label, invalid_branch, protected_branches):
    """Generate a markdown string for the given MR & pipelines."""
    jinja_env = Environment(loader=FileSystemLoader("templates/"),
                            trim_blocks=True, lstrip_blocks=True)
    template = jinja_env.get_template('ckihook.j2')

    header_str = f'{REPORT_HEADER} ~"{cki_status_label}"'
    context = context_dict(pipe_mr, pipelines, header_str, invalid_branch, protected_branches)

    comment_string = template.render(context)
    return '\n'.join([f'{line}  ' for line in comment_string.splitlines()])


def generate_pipeline_labels(pipe_mr, pipelines):
    """Return the set of CKI labels derived from the given dict of PipelineResults."""
    cki_labels = set()
    for pipeline_type, pipeline_result in pipelines.items():
        if pipeline_result is None:
            LOGGER.info('Missing result for %s', repr(pipeline_type))
            cki_labels.add(f'{pipeline_type.prefix}::Missing')
            continue
        if not pipeline_result.label:
            LOGGER.info('%s has no label, ignoring result.', pipeline_result)
            continue
        if pipeline_is_waived(pipeline_result, pipe_mr.labels):
            LOGGER.info('MR has Waived label for %s, ignoring result.', pipeline_result)
            continue
        cki_labels.add(pipeline_result.label)
    LOGGER.info('Calculated CKI labels: %s', cki_labels)
    return cki_labels


def generate_status_label(pipe_mr, pipelines, invalid_branch):
    """Return a string representing the overall status of all required pipelines."""
    default_label = PipelineStatus.INVALID if invalid_branch else PipelineStatus.MISSING
    # Get the lowest PipelineStatus of expected results which are not allowed to fail & not waived.
    lowest_status = min((pipe.status for pipe in pipelines.values() if
                         pipe is not None and
                         not pipe.allow_failure and
                         not pipeline_is_waived(pipe, pipe_mr.labels)),
                        default=default_label)
    return f'CKI::{lowest_status.title}'


def update_mr(session, pipe_mr, pipelines, invalid_branch, protected_branches):
    """Update the given MR with a comment and set CKI labels."""
    cki_status_label = generate_status_label(pipe_mr, pipelines, invalid_branch)
    all_labels = generate_pipeline_labels(pipe_mr, pipelines) | {cki_status_label}
    # Update the MR labels if needed, making sure to remove double-scoped labels.
    if not all_labels.issubset(set(pipe_mr.labels)):
        pipe_mr.add_labels(all_labels, remove_scoped=True)
    else:
        LOGGER.info('MR already has all the expected labels, nothing to do.')

    # Update the report comment.
    report_text = generate_comment(pipe_mr, pipelines, cki_status_label,
                                   invalid_branch, protected_branches)
    LOGGER.info('Leaving comment: \n%s', report_text)
    gl_mr = pipe_mr.gl_mr
    session.update_webhook_comment(gl_mr, report_text,
                                   bot_name=pipe_mr.current_user.username,
                                   identifier=REPORT_HEADER)


def process_pipe_mr(session, pipe_mr: PipeMR) -> None:
    """Populate a sorted dict of the MR's pipelines and call update_mr()."""
    # If the MR state is not 'opened' then don't even bother.
    if pipe_mr.state is not MrState.OPENED:
        LOGGER.info('MR state is %s, nothing to do.', pipe_mr.state.name)
        return
    # If the MR does not have a recognized branch then don't even bother.
    if not pipe_mr.branch:
        LOGGER.info("MR target branch '%s' is not recognized, nothing to do.",
                    pipe_mr.target_branch)
        return
    if pipe_mr.pipelines:
        protected_branches = []
        invalid_branch = None
    else:
        protected_branches = [b.name for b in pipe_mr.gl_project.protectedbranches.list(all=True)]
        invalid_branch = pipe_mr.source_branch if any(
            fnmatch.fnmatchcase(pipe_mr.source_branch, b) for b in protected_branches
        ) else None
    # Create a mapping of PipelineType:PipelineResult pairs.
    pipelines = {}
    for pipeline in pipe_mr.pipelines:
        if pipeline.type in pipelines:
            raise ValueError(f'Multiple pipelines have the same type: {pipeline.type}')
        if pipeline.type == PipelineType.INVALID:
            continue
        pipelines[pipeline.type] = pipeline
    # Insert placeholders keys for any missing pipelines.
    for ptype in pipe_mr.branch.pipelines:
        if ptype not in pipelines:
            pipelines[ptype] = None
    # Sort the pipelines so we process them in a consistent order.
    update_mr(session, pipe_mr, dict(sorted(pipelines.items())), invalid_branch, protected_branches)


def get_pipe_mr(graphql, gl_instance, projects, mr_url):
    """Return a new PipeMR object."""
    return PipeMR(graphql, gl_instance, projects, mr_url)


def process_mr_event(body, session, **_):
    """Process an MR message."""
    mr_url = body['object_attributes']['url']
    LOGGER.info('Processing MR event for %s', mr_url)
    pipe_mr = None
    if branch_changed(body):
        pipe_mr = get_pipe_mr(session.graphql, session.gl_instance, session.rh_projects, mr_url)
        if process_possible_branch_change(session, pipe_mr, body):
            return

    if not cki_label_changed(body['changes']) and \
            misc.get_nested_key(body, 'object_attributes/action') != 'open':
        LOGGER.info('No CKI label changes to make.')
        return

    if not pipe_mr:
        pipe_mr = get_pipe_mr(session.graphql, session.gl_instance, session.rh_projects, mr_url)
    process_pipe_mr(session, pipe_mr)


def process_note_event(body, session, **_):
    """Process a note message."""
    if not common.force_webhook_evaluation(body['object_attributes']['note'],
                                           ['cki', 'ckihook', 'pipeline']):
        LOGGER.info('Note event did not request evaluation, ignoring.')
        return
    mr_url = body['merge_request']['url']
    LOGGER.info('Processing note event for %s', mr_url)
    pipe_mr = get_pipe_mr(session.graphql, session.gl_instance, session.rh_projects, mr_url)
    process_pipe_mr(session, pipe_mr)


def process_pipeline_event(body, session, **_):
    """Process a pipeline message."""
    # This should filter out upstream pipeline events and any downstream pipeline events which are
    # not directly related to an MR.
    if not (mr_url := common.get_pipeline_variable(body, 'mr_url')) or \
       (common.get_pipeline_variable(body, 'CKI_DEPLOYMENT_ENVIRONMENT',
                                     default='production') != 'production'):
        LOGGER.info('Event did not contain the expected variables, ignoring')
        return

    pipeline_name = common.get_pipeline_variable(body, 'trigger_job_name')
    LOGGER.info('Processing pipeline event (id: %s, name: %s) for %s',
                body['object_attributes']['id'], pipeline_name, mr_url)

    pipe_mr = get_pipe_mr(session.graphql, session.gl_instance, session.rh_projects, mr_url)
    process_pipe_mr(session, pipe_mr)


def process_push_event(body, session, **_):
    """Process a push event message."""
    namespace = body['project']['path_with_namespace']
    branch_name = body['ref'].rsplit('/', 1)[-1]
    LOGGER.info('Processing push event for %s on branch %s.', namespace, branch_name)
    if not (branch := session.rh_projects.get_target_branch(body['project']['id'], branch_name)):
        LOGGER.info('Ignoring push to unrecognized branch.')
        return
    if not branch.name.endswith('-rt'):
        LOGGER.info('Ignoring push to non-rt branch.')
        return
    retrigger_failed_pipelines(session.graphql, namespace, branch)


HANDLERS = {
    GitlabObjectKind.MERGE_REQUEST: process_mr_event,
    GitlabObjectKind.NOTE: process_note_event,
    GitlabObjectKind.PIPELINE: process_pipeline_event,
    GitlabObjectKind.PUSH: process_push_event
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('CKIHOOK')
    args = parser.parse_args(args)
    session = new_session('ckihook', args, HANDLERS)
    session.run()


if __name__ == "__main__":
    main(sys.argv[1:])
