"""A session object for webhooks."""
from abc import abstractmethod
from argparse import Namespace
from dataclasses import InitVar
from dataclasses import dataclass
from dataclasses import field
from functools import cached_property
import json
from os import environ
from pathlib import Path
from typing import Optional

from cki_lib import footer
from cki_lib.gitlab import get_instance
from cki_lib.logger import get_logger
from cki_lib.messagequeue import MessageQueue
from cki_lib.metrics import prometheus_init
from cki_lib.misc import deployment_environment
from cki_lib.misc import get_nested_key
from cki_lib.misc import is_production
from cki_lib.misc import is_production_or_staging
from cki_lib.misc import is_staging
from cki_lib.misc import sentry_init
from jira.exceptions import JIRAError
import sentry_sdk

from .common import parse_mr_url
from .common import wait_for_new_mrs
from .defs import GITFORGE
from .defs import GitlabObjectKind
from .defs import JIRA_BOT_ACCOUNTS
from .defs import MessageType
from .graphql import GitlabGraph
from .libjira import connect_jira
from .libjira import get_linked_mrs
from .rh_metadata import Project
from .rh_metadata import Projects
from .rh_metadata import Webhook

LOGGER = get_logger('cki.webhook.session')

SESSION = None


def new_session(webhook_name, parser_args=None, handlers=None, load_policies=False):
    """Return a new BaseSession or FullSession object."""
    if SESSION:
        raise RuntimeError(f'Session already exists! {SESSION}')
    if handlers:
        session = SessionRunner(webhook_name, parser_args, handlers, load_policies=load_policies)
    else:
        session = BaseSession(webhook_name, parser_args, load_policies=load_policies)
    globals()['SESSION'] = session
    return SESSION


def get_session():
    """Return the current Session object or blow up if it isn't set up."""
    if not SESSION:
        raise RuntimeError('No existing Session to get!')
    return SESSION


@dataclass
class BaseSession:
    """An object to hold basic session data."""

    webhook_name: InitVar[str]
    args: Namespace
    load_policies: InitVar[bool] = field(kw_only=True, default=False)
    webhook: Webhook | None = field(init=False, default=None)

    def __post_init__(self, webhook_name, load_policies):
        """Set up the instance."""
        if not (webhook := self.rh_projects.webhooks.get(webhook_name)):
            raise RuntimeError(
                (f"webhook_name '{webhook_name}' not found in Projects data webhooks:"
                 f" {list(self.rh_projects.webhooks.keys())}")
            )
        self.webhook = webhook
        if load_policies:
            self.rh_projects.do_load_policies()
        self.comment = footer.Footer(webhook)
        LOGGER.info('Created %s', self)

    def __repr__(self):
        """Talk about yourself."""
        environment = self.environment.upper() if self.is_production else self.environment
        repr_str = f"hook: '{self.webhook.name}', env: {environment}"
        return f"<{self.__class__.__name__}: {repr_str}>"

    @cached_property
    def gl_hostname(self):
        """Return the hostname used for Gitlab API connections."""
        return GITFORGE

    @cached_property
    def gl_instance(self):
        """Return an authenticated GL REST instance."""
        instance = get_instance(self.gl_hostname)
        instance.auth()
        if not getattr(instance, 'user', None):
            raise RuntimeError('Not authenticated to Gitlab.')
        return instance

    @cached_property
    def graphql(self):
        """Return an authenticated GL graphql instance."""
        graph = GitlabGraph(get_user=True, hostname=self.gl_hostname)
        if not graph.user:
            raise RuntimeError('Not authenticated to Gitlab.')
        return graph

    @cached_property
    def jira(self):
        """Return an authenticated JIRA API instance."""
        jira = connect_jira()
        try:
            jira.myself()
        except JIRAError as err:
            raise RuntimeError('Not authenticated to Jira.') from err
        return jira

    @cached_property
    def rh_projects(self):
        """Return a Projects object."""
        return Projects()

    @property
    def gl_user(self):
        """Return the current GL user the Session is authenticaed as."""
        return self.gl_instance.user

    @cached_property
    def environment(self):
        """Return a string describing the environment."""
        return deployment_environment()

    @cached_property
    def is_production_or_staging(self):
        """Return True if we are running in production or staging, otherwise False."""
        return is_production_or_staging()

    @cached_property
    def is_production(self):
        """Return True if we are running in production, otherwise False."""
        return is_production()

    @cached_property
    def is_staging(self):
        """Return True if we are running in staging, otherwise False."""
        return is_staging()

    def update_webhook_comment(self, gl_mergerequest, text, *, bot_name=None, identifier=None):
        """Find the webhook's status comment so we can edit it in place, add one if none exists."""
        if bot_name and identifier:
            for discussion in gl_mergerequest.discussions.list(iterator=True):
                note = discussion.attributes['notes'][0]
                if note['author']['username'] == bot_name and identifier in note['body']:
                    body = text + self.comment.gitlab_footer('updated')
                    if self.is_production_or_staging:
                        LOGGER.debug('Overwriting existing webhook comment:\n%s', body)
                        comment = discussion.notes.get(note['id'])
                        comment.body = body
                        comment.save()
                    else:
                        LOGGER.debug('Would overwrite existing webhook comment:\n%s', body)
                    return

        body = text + self.comment.gitlab_footer()
        if self.is_production_or_staging:
            LOGGER.debug('Creating new webhook comment:\n%s', body)
            gl_mergerequest.notes.create({'body': body})
        else:
            LOGGER.debug('Would create new webhook comment:\n%s', body)


@dataclass
class SessionRunner(BaseSession):
    """A BaseSession with event handling."""

    handlers: dict

    def __post_init__(self, *args, **kwargs):
        """Set up the Handlers."""
        handlers = {}
        for handler_type, handler_func in self.handlers.items():
            if isinstance(handler_type, GitlabObjectKind):
                handler = HANDLER_CLASSES[MessageType.GITLAB](self, handler_func, handler_type)
            else:
                handler = HANDLER_CLASSES[handler_type](self, handler_func)
            handlers[handler_type] = handler
        self.handlers = handlers
        super().__post_init__(*args, **kwargs)

    def __repr__(self):
        """Talk about yourself."""
        repr_str = super().__repr__()
        handlers = [handler.name for handler in getattr(self, 'handlers').keys()]
        return f'{repr_str[:-1]}, handlers: {handlers}>'

    def run(self):
        """Run it."""
        if self.args.merge_request:
            func = self.process_cmdline_message
        elif self.args.json_message_file:
            func = self.process_json_message
        else:
            func = self.consume_messages
        return func()

    def get_handler(self, message_type_or_object_kind):
        """Return the matching Handler or None."""
        return self.handlers.get(message_type_or_object_kind)

    @staticmethod
    def build_cmdline_message(args):
        """Return a GitlabMsg object matching the command line parameters."""
        if not args.merge_request:
            raise ValueError("'args.merge_request' must be set.")
        if args.action:
            gl_kind = GitlabObjectKind.get(args.action)
        elif args.note:
            gl_kind = GitlabObjectKind.NOTE
        else:
            gl_kind = GitlabObjectKind.MERGE_REQUEST
        msg_dict = {'url': args.merge_request,
                    'object_kind': gl_kind,
                    'project_id': args.payload_project_id,
                    'target_branch': args.payload_target_branch,
                    'note': args.note}
        return GitlabMsg(**msg_dict)

    def process_cmdline_message(self):
        """Set up a fake payload from the given cmdline parameters and process the message."""
        headers = {'message-type': 'gitlab'}
        body = self.build_cmdline_message(self.args).as_payload()
        return self.process_one_message(routing_key='cmdline', headers=headers, body=body)

    def process_json_message(self):
        """Set up a message from input json and process it."""
        headers = {'message-type': self.args.json_message_type}
        file_name = self.args.json_message_file
        msg_json = Path(file_name).read_text(encoding='utf-8')
        body = json.loads(msg_json)
        return self.process_one_message(routing_key=file_name, headers=headers, body=body)

    def process_one_message(self, routing_key=None, headers=None, body=None, **kwargs):
        """Match the event to a Handler and run it if we determine we should."""
        message_type = MessageType.get(headers['message-type'])
        object_kind = GitlabObjectKind.get(body.get('object_kind')) if \
            message_type is MessageType.GITLAB else None
        LOGGER.info('Received message on %s of type %s:\n%s', routing_key,
                    object_kind.name if object_kind else message_type,
                    json.dumps(body, indent=None))
        if not (handler := self.get_handler(object_kind or message_type)):
            LOGGER.info('No handler for this message, nothing to do.')
            return False
        # Call the handler and away we go!
        if handler.should_process_message(headers, body):
            LOGGER.info('Invoking handler function %s …', handler.handler_func)
            handler.handler_func(body, self, **kwargs)
            return True
        LOGGER.info('Handler declined the message, nothing to do.')
        return False

    @cached_property
    def queue(self):
        """Return a MessageQueue object."""
        mqueue = MessageQueue(host=self.args.rabbitmq_host,
                              port=self.args.rabbitmq_port,
                              user=self.args.rabbitmq_user,
                              password=self.args.rabbitmq_password)
        mqueue.msg_logging_env.add_hook(MessageType.AMQP, make_amqp_logging_extras)
        mqueue.msg_logging_env.add_hook(MessageType.UMB_BRIDGE, make_umb_bridge_logging_extras)
        return mqueue

    def consume_messages(self, **kwargs):
        # pylint: disable=unnecessary-lambda
        """Get the MessageQueue and start processing messages."""
        prometheus_init()
        sentry_init(sentry_sdk, ca_certs=self.args.sentry_ca_certs)
        if not self.args.rabbitmq_routing_key:
            raise RuntimeError('--rabbitmq-routing-key must be set.')
        self.queue.consume_messages(
            exchange=self.args.rabbitmq_exchange,
            routing_keys=self.args.rabbitmq_routing_key.split(),
            callback=lambda **cbkwargs: self.process_one_message(**cbkwargs, **kwargs),
            queue_name=self.args.rabbitmq_queue_name
        )


def make_amqp_logging_extras(_, body):
    """Create the dict of 'extras' logging info for an amqp_bridge event."""
    return {'bugzilla_id': get_nested_key(body, 'event/bug_id'),
            'bugzilla_user': get_nested_key(body, 'event/user/login')
            }


def make_umb_bridge_logging_extras(headers, body):
    """Create the dict of 'extras' logging info for an ampq_bridge event."""
    extras = {}
    if body.get('mrpath'):
        extras['mr_id'] = int(body['mrpath'].split('!')[-1])
        extras['path_with_namespace'] = body['mrpath'].split('!')[0]
    if headers.get('source'):
        extras['event_source'] = headers['source']
    return extras


@dataclass
class GitlabMsg:
    """Creates a gitlab webhook event message."""

    url: str
    object_kind: GitlabObjectKind = GitlabObjectKind.MERGE_REQUEST
    project_id: int = 123
    target_branch: str = 'main'
    note: str = ''

    def as_payload(self):
        """Return the payload as a dict."""
        payload = {'object_kind': self.object_kind.name.lower()}
        if self.object_kind is not GitlabObjectKind.BUILD:
            payload['project'] = self.project_dict
        if self.object_kind is not GitlabObjectKind.PUSH:
            payload['user'] = {'username': 'cli', 'id': 0}

        match self.object_kind:
            case GitlabObjectKind.MERGE_REQUEST:
                payload.update(
                    {'changes': {},
                     'labels': [],
                     'object_attributes': self.merge_request_dict}
                )
            case GitlabObjectKind.NOTE:
                payload.update(
                    {'merge_request': self.merge_request_dict,
                     'object_attributes': {'noteable_type': 'MergeRequest', 'note': self.note}}
                )
            case GitlabObjectKind.PIPELINE:
                payload.update(
                    {'merge_request': self.merge_request_dict}
                )
            case GitlabObjectKind.PUSH:
                payload.update(
                    {'ref': f'refs/head/{self.target_branch}',
                     'user_id': 0,
                     'user_username': 'cli'}
                )
        return payload

    @property
    def iid(self):
        """Return the MR ID as an int."""
        return parse_mr_url(self.url)[1]

    @property
    def path_with_namespace(self):
        """Return the path_with_namespace string."""
        return parse_mr_url(self.url)[0]

    @property
    def project_name(self):
        """Return the project name string."""
        return self.path_with_namespace.rsplit('/', 1)[-1]

    @property
    def merge_request_dict(self):
        """Return the merge_request dict."""
        mr_dict = {'iid': self.iid,
                   'state': 'opened',
                   'target_branch': self.target_branch,
                   'work_in_progress': False,
                   'url': self.url
                   }
        if self.object_kind is GitlabObjectKind.MERGE_REQUEST:
            mr_dict.update({'action': 'open', 'head_pipeline_id': None})
        return mr_dict

    @property
    def project_dict(self):
        """Return the project dict."""
        scheme = self.url.split(':', 1)[0]
        netloc = self.url.removeprefix(f'{scheme}://').split('/', 1)[0]
        return {'id': self.project_id,
                'name': self.project_name,
                'description': 'A faux project',
                'web_url': f'{scheme}://{netloc}/{self.path_with_namespace}',
                'git_ssh_url': f'git@{netloc}:{self.path_with_namespace}.git',
                'git_http_url': f'{scheme}://{netloc}/{self.path_with_namespace}.git',
                'namespace': self.path_with_namespace,
                'visibility_level': 0,
                'path_with_namespace': self.path_with_namespace,
                'default_branch': 'main',
                "ci_config_path": ""
                }


class MessageHandler:
    """Base class for message handlers."""

    type = None

    def __init__(self, session, handler_func):
        """Set up and announce the object."""
        self.session = session
        self.handler_func = handler_func
        LOGGER.info('Created %s', self)

    def __repr__(self):
        """Represent."""
        repr_str = f'func: {self.handler_func}'
        if hasattr(self, 'object_kind'):
            repr_str = f'{self.object_kind.name}, ' + repr_str
        return f'<{self.__class__.__name__}: {repr_str}>'

    @abstractmethod
    def event_project(self, body):
        """Return the GL Project object associated with the event, or None if no match is found.

        Returning True with cause the project/webhook check being skipped for event types
        which are not associated with a GL project.
        """

    @abstractmethod
    def event_user(self, body):
        """Return the username who generated the given event, or None if no such property exists."""

    @abstractmethod
    def filter_message(self, headers, body):
        """Return True if the message should be handled, otherwise False."""
        # This is called by process_message to provide extra filtering per handler.

    def should_process_message(self, headers, body):
        """Return True if the message should be handled, otherwise False."""
        result = True
        # Filter out known bot messages.
        event_user = self.event_user(body)
        if not self.session.args.disable_user_check and event_user in self.users_to_ignore:
            LOGGER.info("Event is from ignorable user '%s', ignoring.", event_user)
            result = False
        # Don't try to validate the GL project referenced in the MR has this hook enabled in
        # the rh_metadata Project. This is for hooks like sprinter and terminator that may receive
        # events from outside the normal KWF projects.
        elif not self.session.webhook.match_to_projects:
            LOGGER.info('Skipping Project check for this webhook.')
        # Fitler out messages from unexpected Projects.
        elif not (event_project := self.event_project(body)):
            LOGGER.info('Event is not associated with any known Project, ignoring.')
            result = False
        elif isinstance(event_project, Project):
            LOGGER.info('Event associated with project %s (sandbox: %s)', event_project.namespace,
                        event_project.sandbox)
            # Filter out sandbox-project messages in production, and non-sandbox-project messages
            # in staging.
            if not self.session.args.disable_environment_check:
                if self.session.is_production and event_project.sandbox:
                    LOGGER.info('Ignoring sandbox project event while in production environment.')
                    result = False
                elif self.session.is_staging and not event_project.sandbox:
                    LOGGER.info('Ignoring production project event while in staging environment.')
                    result = False
            # Filter out messages for hooks which are not active in this Project.
            if result and self.session.webhook.name not in event_project.webhooks:
                LOGGER.info("'%s' is not an expected webhook for this event's project: %s",
                            self.session.webhook.name, event_project)
                result = False
        if result:
            result = self.filter_message(headers, body)
        return result

    @property
    def users_to_ignore(self):
        """Return the list of users to ignore events from."""
        return []


class GitlabHandler(MessageHandler):
    """A message handler for Gitlab webhook events."""

    type = MessageType.GITLAB

    def __init__(self, session, handler_func, object_kind):
        """Set the GitlabObjectKind."""
        self.object_kind = object_kind
        super().__init__(session, handler_func)

    def event_project(self, body: dict) -> Optional[Project]:
        """Return the Project object derived from the event, or None if nothing can be found."""
        project: Optional[Project] = None
        namespace: Optional[str] = None
        # Try to get the path_with_namespace from the event.
        # Build (job) and Pipeline events from downstream should have a 'source_pipeline' key
        # which for them we prefer over the 'project' key.
        if self.object_kind in (GitlabObjectKind.BUILD, GitlabObjectKind.PIPELINE):
            namespace = get_nested_key(body, 'source_pipeline/project/path_with_namespace')
        # For everything else (including builds/pipelines without 'source_pipeline') look
        # at the 'project' key.
        if not namespace:
            namespace = get_nested_key(body, 'project/path_with_namespace')
        # If we have the namespace then see if we have Project for it.
        if namespace:
            project = self.session.rh_projects.get_project_by_namespace(namespace)
        elif self.object_kind is GitlabObjectKind.BUILD:
            # Build (job) events will always have a 'project_id' we can try...
            if project_id := body.get('project_id'):
                project = self.session.rh_projects.get_project_by_id(project_id)
        if not project:
            LOGGER.warning('Unable to match event data to a rh_metadata project!')
        return project

    def event_user(self, body):
        """Return the username of the user who generated this event."""
        return get_nested_key(body, 'user/username') or body.get('user_username')

    def filter_message(self, _, body):
        """Return True if the MessageType.GITLAB message should be sent to the handler."""
        if self.object_kind is GitlabObjectKind.NOTE:
            # Always ignore Note Events if they are not associated with an MR.
            if body['object_attributes'].get('noteable_type') != 'MergeRequest':
                LOGGER.info('Note event is not related to an MR, ignoring.')
                return False

        # Ignore events from all closed/merged MRs except for the actual closing notification.
        if not self.session.args.disable_closed_status_check:
            state = get_nested_key(body, 'object_attributes/state') or \
                get_nested_key(body, 'merge_request/state')
            action = get_nested_key(body, 'object_attributes/action')
            if state in ('closed', 'merged') and action != 'close':
                LOGGER.info("Ignoring event with '%s' state and '%s' action.", state, action)
                return False
        wait_for_new_mrs(body)
        return True

    @property
    def users_to_ignore(self):
        """Return a list with the username we are logged into the API as."""
        # If ckihook has retriggered a pipeline then all the events related to it will show an
        # event_user which matches our session user. But we still want to process these events and
        # really there is no reason to ignore any build or pipeline event because of the username
        # so just don't ignore anyone in this case.
        if self.object_kind in (GitlabObjectKind.BUILD, GitlabObjectKind.PIPELINE):
            return []
        return [self.session.gl_user.username]


class AmqpHandler(MessageHandler):
    """A message handler for amqp message events from the UMB (bugzilla)."""

    type = MessageType.AMQP

    def event_project(self, body):
        """Return True since UMB bugzilla events are not directly associated with RH projets."""
        return True

    def event_user(self, body):
        """Return the bugzilla account (email) associated with the event."""
        return get_nested_key(body, 'event/user/login')

    def filter_message(self, _, body):
        """Return True if the message should be sent to the handler."""
        # All we can really do here is confirm there is a bug_id.
        if not get_nested_key(body, 'event/bug_id'):
            LOGGER.info('Bugzilla UMB event has no bug_id, ignoring.')
            return False
        return True

    @property
    def users_to_ignore(self):
        """Return a list with the email from BUGZILLA_EMAIL environ."""
        return [environ.get('BUGZILLA_EMAIL')]


class JiraHandler(MessageHandler):
    """A message handler for Jira webhook events."""

    type = MessageType.JIRA

    def event_user(self, body):
        """Return the username associated with the event."""
        return get_nested_key(body, 'user/name')

    def event_project(self, body):
        """Return the Project associated with the event, or None."""
        # A jira issue could be linked to multiple MRs but here we just return the first
        # matching Project 🤷.
        # Only consider issues in the RHEL space.
        if not get_nested_key(body, 'issue/key', '').startswith('RHEL-'):
            LOGGER.info('Event is not from a RHEL issue, ignoring.')
            return None
        if jissue_id := get_nested_key(body, 'issue/key'):
            linked_mrs = get_linked_mrs(jissue_id, jiracon=self.session.jira)
            for mr_url in linked_mrs:
                namespace, _ = parse_mr_url(mr_url)
                if project := self.session.rh_projects.get_project_by_namespace(namespace):
                    return project
        return None

    def filter_message(self, *_):
        """Return True if the message should be sent to the handler."""
        # Nothing extra to do.
        return True

    @property
    def users_to_ignore(self):
        """Return the list of known bot accounts."""
        return JIRA_BOT_ACCOUNTS


class UmbBridgeHandler(MessageHandler):
    """A message handler for events from the UMB Bridge."""

    type = MessageType.UMB_BRIDGE

    def event_user(self, _):
        # pylint: disable=no-self-use
        """Return None as these events are coming from inside the house."""
        return None

    def event_project(self, body):
        """Return the Project associated with the event."""
        namespace = body['mrpath'].split('!')[0]
        return self.session.rh_projects.get_project_by_namespace(namespace)

    def filter_message(self, headers, _):
        """Return True if the message should be processed."""
        # The umb-bridge includes the name of the hook which should process the event in the header.
        event_target = headers.get('event_target_webhook')
        LOGGER.info('event_target is %s, webhook is: %s', event_target, self.session.webhook.name)
        if event_target != self.session.webhook.name:
            LOGGER.info("Event target '%s' does not match environment.", event_target)
            return False
        return True


HANDLER_CLASSES = {
    MessageType.GITLAB: GitlabHandler,
    MessageType.AMQP: AmqpHandler,
    MessageType.JIRA: JiraHandler,
    MessageType.UMB_BRIDGE: UmbBridgeHandler
}
