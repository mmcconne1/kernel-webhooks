"""GraphQL Fragments to use with queries.

See https://graphql.org/learn/queries/#fragments

"""

CKI_PIPELINE = """
fragment CkiPipeline on Pipeline {
  id
  createdAt
  project {
    id
  }
  sourceJob {
    name
  }
  status
  stages {
    nodes {
      name
      jobs {
        nodes {
          status
        }
      }
    }
  }
}
"""

CURRENT_USER = """
fragment CurrentUser on Query {
  currentUser {
    ...GlUser
  }
}
"""

GL_USER = """
fragment GlUser on User {
  gid: id
  name
  email: publicEmail
  username
}
"""

MR_COMMITS = """
fragment MrCommits on MergeRequest {
  commits: commitsWithoutMergeCommits(after: $after) {
    pageInfo {
      hasNextPage
      endCursor
    }
    nodes {
      author {
        ...GlUser
      }
      authorEmail
      authorName
      authoredDate
      description
      sha
      title
    }
  }
}
"""

MR_FILES = """
fragment MrFiles on MergeRequest {
  files: diffStats {
    path
  }
}
"""

MR_LABELS = """
fragment MrLabels on MergeRequest {
  labels {
    nodes {
      title
    }
  }
}
"""

MR_LABELS_PAGED = """
fragment MrLabelsPaged on MergeRequest {
  labels(after: $after) {
    pageInfo {
      hasNextPage
      endCursor
    }
    nodes {
      title
    }
  }
}
"""
