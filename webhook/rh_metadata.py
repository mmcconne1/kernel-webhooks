"""Metadata for RHEL projects."""
from dataclasses import dataclass
from dataclasses import field
from dataclasses import fields
from functools import cached_property
from os import environ
from typing import GenericAlias
from typing import Iterable
from typing import Optional
from typing import Self
from typing import TYPE_CHECKING
from typing import Union

from cki_lib import footer
from cki_lib.logger import get_logger
from cki_lib.yaml import load

from webhook.cpc import get_policy_data
from webhook.defs import DevStage
from webhook.defs import FixVersion
from webhook.defs import Label
from webhook.defs import MrScope
from webhook.pipelines import PipelineType

if TYPE_CHECKING:
    from webhook.cpc import Token

LOGGER = get_logger(__name__)


def check_data(this_dc: dataclass) -> None:
    """Check the dataclass object fields have the right types and are not empty values."""
    dc_type = type(this_dc)
    for dc_field in [field for field in fields(this_dc) if field.init]:
        fvalue = getattr(this_dc, dc_field.name)
        field_base_type = dc_field.type.__origin__ if isinstance(dc_field.type, GenericAlias) else \
            dc_field.type
        # fields should hold data of the correct type
        if not isinstance(fvalue, field_base_type):
            raise TypeError(f'{dc_type}.{dc_field.name} must be {field_base_type}: {this_dc}')
        # do not test for empty values if a field is a bool or has an empty default value
        if isinstance(fvalue, bool) or (isinstance(fvalue, str) and dc_field.default == '') or \
           (isinstance(fvalue, list) and dc_field.default_factory is list):
            continue
        # fields with non-empty default values should not have empty values
        if not fvalue:
            raise ValueError(f'{dc_type}.{dc_field.name} cannot be empty: {this_dc}')


@dataclass(kw_only=True)
class Webhook(footer.Context):  # pylint: disable=too-many-instance-attributes
    """Webhook metadata."""

    required: bool = True
    required_label_prefixes: list[str] = field(default_factory=list)
    extra_labels_regex: str = ''
    required_for_qa_scope: MrScope = MrScope.NEEDS_REVIEW
    on_draft: bool = True
    match_to_projects: bool = True
    request_evaluation_triggers: list[str] = field(default_factory=list)
    run_on_drafts: bool = True
    run_on_blocking_mismatch: bool = False
    run_on_changed_commits: bool = True
    run_on_changed_description: bool = True
    run_on_changed_to_draft: bool = False
    run_on_changed_to_ready: bool = True
    run_on_closing: bool = False
    note_text_patterns: list[str] = field(default_factory=list)
    skip_session_filter_check: bool = False
    skip_session_trigger_check: bool = False
    instance_manages_ready_labels: bool = False

    def __post_init__(self) -> None:
        """Fix the required_for_qa_scope and Check it."""
        if not isinstance(self.required_for_qa_scope, MrScope):
            self.required_for_qa_scope = MrScope.get(self.required_for_qa_scope)
        check_data(self)

    def status(self, mr_labels: Iterable[Union[str, Label]]) -> MrScope:
        """Return the MrScope describing this hook's status for the given set of labels."""
        if not self.required:
            return MrScope.READY_FOR_MERGE
        status = MrScope.NEEDS_REVIEW
        if hook_mr_labels := [label for label in mr_labels if
                              label.scoped and label.prefix in self.required_label_prefixes]:
            if all(label.scope >= self.required_for_qa_scope for label in hook_mr_labels):
                status = MrScope.READY_FOR_QA
            if all(label.scope >= MrScope.OK for label in hook_mr_labels):
                status = MrScope.READY_FOR_MERGE
        return status


@dataclass(kw_only=True)
class Project:
    # pylint: disable=too-many-instance-attributes
    """Project metadata."""

    id: int  # pylint: disable=invalid-name
    group_id: int
    name: str
    product: str
    pipelines: list[PipelineType] = field(default_factory=list, repr=False)
    inactive: bool = False
    sandbox: bool = False
    confidential: bool = False
    group_labels: bool = True
    namespace: str = ''
    branches: list['Branch'] = field(default_factory=list, repr=False)
    webhooks: dict[str, Webhook] = field(default_factory=dict, repr=False)
    public_signoff_ok: bool = False

    def __post_init__(self) -> None:
        """Set up branches & pipelines and check data."""
        self.pipelines = [PipelineType.get(p) for p in self.pipelines]
        self.branches = [Branch(**b, project=self) for b in self.branches]
        self.webhooks = {w['name']: Webhook(**w) for w in self.webhooks.values()}
        check_data(self)

    def get_branch_by_name(self, branch_name: str) -> Union['Branch', None]:
        """Return the branch with the given name, or None."""
        return next((b for b in self.branches if b.name == branch_name), None)

    def get_branches_by_itr(self, itr: str) -> list['Branch']:
        """Return a list of active branches whose policy matches the given ITR."""
        return [b for b in self.branches if b.internal_target_release == itr and not b.inactive]

    def get_branches_by_ztr(self, ztr: str) -> list['Branch']:
        """Return a list of active branches whose policy matches the given ZTR."""
        return [b for b in self.branches if b.zstream_target_release == ztr and not b.inactive]

    def get_branches_by_fix_version(self, fixver: FixVersion) -> list['Branch']:
        """Return a list of active branches whose policy matches the given fix_version."""
        return [b for b in self.branches if fixver in b.fix_versions and not b.inactive]

    def webhooks_status(self, mr_labels: Iterable[Union[str, Label]]) -> MrScope:
        """Return the minimim MrScope of this project's webhook labels for the given mr_labels."""
        mr_labels = [Label(label) for label in mr_labels]
        return min(webhook.status(mr_labels) for webhook in self.webhooks.values())


@dataclass(eq=True, kw_only=True)
class Branch:
    # pylint: disable=too-many-instance-attributes
    """Branch metadata."""

    name: str
    components: set
    distgit_ref: str
    sub_component: str = ''
    internal_target_release: str = ''
    zstream_target_release: str = ''
    milestone: str = ''
    inactive: bool = False
    pipelines: list[PipelineType] = field(default_factory=list, repr=False)
    policy: list['Token'] = field(default_factory=list, compare=False, repr=False)
    project: Project | None = field(default=None, repr=False)
    protected_approval_rules: bool = True
    fix_versions: list = field(default_factory=list)

    def __post_init__(self) -> None:
        """Set up pipelines and check data."""
        if self.pipelines:
            self.pipelines[:] = [PipelineType.get(pipe) if isinstance(pipe, str) else pipe for
                                 pipe in self.pipelines]
        if not self.pipelines:
            self.pipelines = self.project.pipelines
        if self.project.inactive:
            self.inactive = True
        self.components = set(self.components)
        self.fix_versions = [FixVersion(fv_str) for fv_str in self.fix_versions]
        check_data(self)

    @cached_property
    def major(self) -> Union[int, None]:
        """Return the Major version number, or None if not present."""
        return self.fix_versions[0].major if self.fix_versions else None

    @cached_property
    def minor(self) -> Union[int, None]:
        """Return the Minor version number, or None if not present."""
        return self.fix_versions[0].minor if self.fix_versions else None

    @cached_property
    def ystream(self) -> Union[bool, None]:
        """True if any of the fix_versions are ystream, or None if not present."""
        return any(not fv.zstream for fv in self.fix_versions) if self.fix_versions else None

    @cached_property
    def zstream(self) -> Union[bool, None]:
        """True if any of the fix_versions are zstream, or None if not present."""
        return any(fv.zstream for fv in self.fix_versions) if self.fix_versions else None

    @cached_property
    def stage(self) -> DevStage:
        """Return the DevStage representing the development stage of this Branch."""
        if self.fix_versions:
            if any(fv.cycle == 'alpha' for fv in self.fix_versions):
                return DevStage.ALPHA
            if any(fv.cycle == 'beta' for fv in self.fix_versions):
                return DevStage.BETA
            # During early zstream stage a branch will have both a ystream and zstream style
            # fix version.
            if self.ystream and self.zstream:
                return DevStage.EARLY_ZSTREAM
            if self.ystream:
                return DevStage.YSTREAM
            if self.zstream:
                return DevStage.ZSTREAM
        return DevStage.UNKNOWN

    @cached_property
    def version(self) -> Union[str, None]:
        """Return a version string derived from fix_version, or None."""
        if not self.fix_versions:
            return None
        return f'{self.fix_versions[0].major}.{self.fix_versions[0].minor}'

    def __ge__(self, other: Self) -> bool:
        """Return True if self is greather than or equal to other, otherwise False."""
        return self.__gt__(other) or self == other

    def __gt__(self, other: Self) -> bool:
        """Return True if self is greater than other, otherwise False."""
        if other.stage is self.stage:
            return (self.major, self.minor) > (other.major, other.minor)
        return self.stage > other.stage

    def __le__(self, other: Self) -> bool:
        """Return True if self is less than or equal to other, otherwise False."""
        return self.__lt__(other) or self == other

    def __lt__(self, other: Self) -> bool:
        """Return True if self is less than other, otherwise False."""
        if other.stage is self.stage:
            return (self.major, self.minor) < (other.major, other.minor)
        return self.stage < other.stage


class Projects:
    """Projects metadata."""

    def __init__(self, load_policies: bool = False, yaml_path: Optional[str] = None) -> None:
        """Load yaml into Projects and check data."""
        if not yaml_path:
            if not (yaml_path := environ.get('RH_METADATA_YAML_PATH')):
                raise RuntimeError(
                    ("Projects() must be called with either the 'yaml_path' parameter set or the"
                     " RH_METADATA_YAML_PATH environment variable set.")
                )
        raw_data = load(file_path=yaml_path)
        self.projects = {raw_proj['id']: Project(**raw_proj) for raw_proj in raw_data['projects']}
        self.webhooks = {wh['name']: Webhook(**wh) for wh in raw_data['webhooks'].values()}
        if load_policies:
            self.do_load_policies()

    @staticmethod
    def _format_project_id(project_id: Union[str, int]) -> int:
        """Return the project_id as an int."""
        # project_id can be a number or a Gitlab ID scalar type, ie. gid://gitlab/Project/1234567
        if isinstance(project_id, str) and project_id.startswith('gid://gitlab/Project/'):
            project_id = project_id.removeprefix('gid://gitlab/Project/')
        return int(project_id)

    def get_project_by_id(self, project_id: Union[str, int]) -> Union[Project, None]:
        """Return the project with the given project_id, or None."""
        project_id = self._format_project_id(project_id)
        return self.projects.get(project_id)

    def get_projects_by_name(self, project_name: str) -> list[Project]:
        """Return the list of Projects with the given name."""
        return [project for project in self.projects.values() if project.name == project_name]

    def get_project_by_namespace(self, namespace: str) -> Union[Project, None]:
        """Return the Project object with the matching namespace."""
        return next((project for project in self.projects.values() if
                     project.namespace == namespace), None)

    def get_target_branch(
        self,
        project_id: Union[str, int],
        target_branch: str
    ) -> Union[Branch, None]:
        """Return the Branch object matching the given project_id and target_branch."""
        project_id = self._format_project_id(project_id)
        if not (project := self.projects.get(project_id)):
            return None
        return next((branch for branch in project.branches if branch.name == target_branch), None)

    def do_load_policies(self) -> None:
        """Load policy tokens for each branch."""
        policies = get_policy_data(policy_regex=r'^(rhel-.*|c\d+s)$')
        for project in self.projects.values():
            for branch in project.branches:
                if policy := policies.get(branch.distgit_ref):
                    branch.policy = policy


def is_branch_active(projects: Projects, project_id: Union[str, int], target_branch: str) -> bool:
    """Return True if the target branch is active, otherwise False."""
    if project := projects.get_project_by_id(project_id):
        if branch := project.get_branch_by_name(target_branch):
            return not branch.inactive
    else:
        LOGGER.warning('Project %s is not present in rh_metadata.', project_id)
    return False
