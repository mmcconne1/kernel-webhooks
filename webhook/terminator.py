"""Cancel Beaker/AWS jobs for finished GitLab jobs."""
import json
import os
import re
import subprocess
import sys
import xml.etree.ElementTree as ET

import boto3
from cki_lib import logger
from cki_lib import misc
from cki_lib.gitlab import parse_gitlab_url
from gitlab.exceptions import GitlabGetError
import prometheus_client

from . import common
from .defs import GitlabObjectKind
from .session import new_session

LOGGER = logger.get_logger('cki.webhook.terminator')

# Beaker uses cancelled with two L
METRIC_TERMINATOR_BEAKER_JOB_CANCELLED = prometheus_client.Counter(
    'cki_terminator_beaker_job_cancelled', 'Number of Beaker jobs cancelled',
    ['job_status'])
METRIC_TERMINATOR_AWS_RESOURCE_RETURNED = prometheus_client.Counter(
    'cki_terminator_aws_resource_returned', 'Number of AWS machines returned',
    ['job_status'])
# GitLab uses canceled with one L
METRIC_TERMINATOR_CHILD_PIPELINE_CANCELED = prometheus_client.Counter(
    'cki_terminator_child_pipeline_canceled', 'Number of GitLab child pipelines canceled',
    ['parent_status', 'child_status'])


def get_aws_resources(job_id=None):
    """Get a mapping of relevant AWS resource IDs and pipelines.

    If no job ID is specified, resources belonging to all pipelines are returned.
    """
    machines = {}
    for reservation in boto3.client('ec2').describe_instances()['Reservations']:
        for machine in reservation['Instances']:
            for tag in machine.get('Tags', []):
                if tag['Key'] == 'CkiGitLabJobId':
                    with misc.only_log_exceptions():
                        if int(tag['Value']) == job_id or job_id is None:
                            machines[machine['InstanceId']] = tag['Value']

    return machines


class Terminator:
    """Cancel Beaker/AWS jobs for finished GitLab jobs and parent pipelines."""

    def __init__(self, parsed_args):
        """Construct a new instance."""
        self.parsed_args = parsed_args
        self.irc_bot_exchange = parsed_args.irc_bot_exchange
        if self.irc_bot_exchange:
            self.irc_messagequeue = common.get_messagequeue(parsed_args, keepalive_s=5)
        else:
            self.irc_messagequeue = None
        self.running_jobs = {}

        # Beaker specific information
        self.beaker_url = parsed_args.beaker_url
        self.bkr_binary = parsed_args.bkr_binary
        if parsed_args.beaker_owner:
            self.beaker_owner_filter_args = ['--owner', re.sub('@.*', '', parsed_args.beaker_owner)]
        else:
            self.beaker_owner_filter_args = []

    def run(self):
        """Run the main loop."""
        session = new_session('terminator', self.parsed_args,
                              {GitlabObjectKind.PIPELINE: self.process_pipeline,
                               GitlabObjectKind.BUILD: self.process_job})
        session.run()

    def _bkr(self, args):
        return subprocess.run([self.bkr_binary] + args, check=True, capture_output=True).stdout

    def get_gitlab_job_id_from_whiteboard(self, beaker_job_id):
        """Get the GitLab job ID from Beaker job.

        If no ID is found, None is returned.
        """
        print(f'  getting results for {beaker_job_id}')
        root = ET.fromstring(self._bkr(['job-results', beaker_job_id]))
        whiteboard = root.find('./whiteboard')
        if whiteboard is None:
            LOGGER.warning('No whiteboard found for Beaker job %s', beaker_job_id)
            return None

        try:
            json_whiteboard = json.loads(whiteboard.text)
        except json.JSONDecodeError:
            LOGGER.error('Job whiteboard for %s is not a valid json!', beaker_job_id)
            return None

        gitlab_job_id = json_whiteboard.get('job_id')
        if not gitlab_job_id:
            LOGGER.warning('No GitLab job ID found for Beaker job %s', beaker_job_id)
            return None

        return int(gitlab_job_id)

    def process_job(self, body, session, **_):
        """Process a job event."""
        if body['build_stage'] != 'test':
            return
        if body['build_status'] not in ('success', 'failed', 'canceled'):
            return

        job_id = body['build_id']

        # Reconstruct the job URL now to avoid doing it multiple times for the
        # same job later on.
        gl_project = session.gl_instance.projects.get(body['project_id'], lazy=True)
        gl_job = gl_project.jobs.get(job_id)
        job_url = gl_job.web_url

        self.stop_beaker_jobs(job_id, job_url, body['build_status'])
        self.stop_aws_jobs(job_id, job_url, body['build_status'])

    def process_pipeline(self, body, session, **_):
        """Process a pipeline event."""
        parent_pipeline_id = body['object_attributes']['id']
        gl_parent_project = session.gl_instance.projects.get(body['project']['id'], lazy=True)
        if not (gl_parent_pipeline := common.get_pipeline(gl_parent_project, parent_pipeline_id)):
            return

        if gl_parent_pipeline.status != 'canceled':
            return

        for gl_bridge in gl_parent_pipeline.bridges.list(all=True):
            child_pipeline = gl_bridge.downstream_pipeline
            if child_pipeline and child_pipeline['status'] not in ('success', 'failed', 'canceled'):
                gl_project = \
                    session.gl_instance.projects.get(child_pipeline['project_id'], lazy=True)
                if not (gl_pipeline := common.get_pipeline(gl_project, child_pipeline['id'])):
                    return
                METRIC_TERMINATOR_CHILD_PIPELINE_CANCELED.labels(
                    parent_status=gl_parent_pipeline.status,
                    child_status=gl_pipeline.status
                ).inc()
                self.cancel_child_pipeline(gl_pipeline, gl_parent_pipeline)

    def stop_beaker_jobs(self, gitlab_job_id, gitlab_job_url, status):
        """For a finished pipeline, make sure all Beaker jobs are stopped."""
        # We cannot do 'AND' condition for multiple whiteboard contains from
        # the cmdline, and trying to hardcode the json element string to match
        # is not exactly robust, so grab the Beaker jobs that have the
        # gitlab_job_id and later verify it is indeed the job ID and not e.g.
        # the pipeline ID.
        jobs = json.loads(self._bkr(['job-list',
                                     '--whiteboard', str(gitlab_job_id),
                                     '--unfinished'] + self.beaker_owner_filter_args))
        for job in jobs:
            job_id_from_whiteboard = self.get_gitlab_job_id_from_whiteboard(job)
            if job_id_from_whiteboard == gitlab_job_id:
                METRIC_TERMINATOR_BEAKER_JOB_CANCELLED.labels(job_status=status).inc()
                self.return_machine(job, gitlab_job_url)

    def stop_aws_jobs(self, gitlab_job_id, gitlab_job_url, status):
        """For a finished pipeline, make sure all AWS resources are returned."""
        for machine in get_aws_resources(job_id=gitlab_job_id):
            METRIC_TERMINATOR_AWS_RESOURCE_RETURNED.labels(job_status=status).inc()
            self.return_machine(machine, gitlab_job_url)

    def load_running_beaker_jobs(self):
        """Retrieve information about running Beaker jobs."""
        print('Loading running Beaker jobs')
        jobs = json.loads(self._bkr(['job-list',
                                     '--whiteboard', 'cki@gitlab',
                                     '--unfinished'] + self.beaker_owner_filter_args))
        for job in jobs:
            if gitlab_job_id := self.get_gitlab_job_id_from_whiteboard(job):
                self.running_jobs[job] = gitlab_job_id

    def load_running_aws_machines(self):
        """Retrieve information about running AWS machines."""
        print('Loading running AWS machines')
        self.running_jobs = get_aws_resources()

    def process_pipeline_project(self, project_url):
        """Process a pipeline repo."""
        _, gl_project = parse_gitlab_url(project_url)

        failures = 0
        for resource, gitlab_job_id in self.running_jobs.items():
            print(f'Trying to find owner of the running resource {resource}')

            try:
                gl_job = gl_project.jobs.get(gitlab_job_id)
            except GitlabGetError:
                print(f'  No GitLab job {gitlab_job_id} found')
                continue

            if gl_job.status not in ('success', 'failed', 'canceled'):
                print(f'  job {gitlab_job_id} not finished')
                continue

            try:
                self.return_machine(resource, gl_job.web_url, output='print')
            except subprocess.CalledProcessError:
                LOGGER.warning('Unable to return resource %s', resource)
                failures += 1
        return failures

    def return_machine(self, resource: str, full_job_url, output='log'):
        """Return a Beaker or AWS resource."""
        status = 'Cancelling' if misc.is_production_or_staging() else 'Detected'
        job_url = full_job_url
        message = f'{status} running machine {resource} of finished job {job_url}'

        if resource.startswith('J:'):
            if self.beaker_url:
                beaker_url = resource.replace('J:', self.beaker_url + '/')
                message += f' Beaker {beaker_url}'

        if output == 'log':
            LOGGER.info('%s', message)
        if output == 'print':
            print('  ' + message)
        if self.irc_messagequeue:
            self.irc_messagequeue.send_message(
                {'message': f'👻 {message}'}, 'irc.message', self.irc_bot_exchange)

        if misc.is_production_or_staging():
            if resource.startswith('J:'):
                self._bkr(['job-cancel', '--msg', json.dumps({
                    'origin': 'terminator',
                    'reason': f'GitLab job {job_url} finished',
                    'retry': False,
                }), resource])
            else:
                boto3.client('ec2').terminate_instances(InstanceIds=[resource])

    def cancel_child_pipeline(self, gl_pipeline, gl_parent_pipeline, output='log'):
        """Cancel a GitLab child pipeline."""
        status = 'Cancelling' if misc.is_production_or_staging() else 'Detected'
        pipeline_url = gl_pipeline.web_url
        parent_pipeline_url = gl_parent_pipeline.web_url
        message = (f'👻 {status} running child pipeline '
                   f'{gl_pipeline.id} of finished parent pipeline {gl_parent_pipeline.id} - '
                   f'child {pipeline_url} parent {parent_pipeline_url}')
        if output == 'log':
            LOGGER.info('%s', message)
        if output == 'print':
            print('  ' + message)
        if self.irc_messagequeue:
            self.irc_messagequeue.send_message(
                {'message': message}, 'irc.message', self.irc_bot_exchange)
        if misc.is_production_or_staging():
            gl_pipeline.cancel()


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('TERMINATOR')
    parser.add_argument('--pipeline-project-url', action='append', default=[],
                        help='Process jobs for a given pipeline project only')
    parser.add_argument('--irc-bot-exchange', default=os.environ.get('IRC_BOT_EXCHANGE'),
                        help='AMQP exchange to send messages to the IRC bot')

    # Beaker reaper
    parser.add_argument('--beaker-url', default=os.environ.get('BEAKER_URL'),
                        help='Beaker URL for linking Beaker jobs')
    parser.add_argument('--bkr-binary', default='bkr',
                        help='path to the bkr binary')
    parser.add_argument('--beaker-owner', default=os.environ.get('BEAKER_OWNER'),
                        help='Owner filter to select Beaker jobs')

    parsed_args = parser.parse_args(args)

    # Always run with the bot user check disabled to allow handling of bot
    # triggered pipelines.
    parsed_args.disable_user_check = True
    # Disable kernel branch checks as (1) we *want* to cancel pipelines there
    # and (2) usually operate on non-kernel projects.
    parsed_args.disable_inactive_branch_check = True

    terminator = Terminator(parsed_args)
    if parsed_args.pipeline_project_url:
        terminator.load_running_beaker_jobs()
        terminator.load_running_aws_machines()
        failures = 0
        for pipeline_project_url in parsed_args.pipeline_project_url:
            failures += terminator.process_pipeline_project(pipeline_project_url)
        if failures:
            LOGGER.error('Some runs could not be cancelled')
            sys.exit(1)
    else:
        terminator.run()


if __name__ == "__main__":
    main(sys.argv[1:])
